# Como usar o SMASH Python

### 1. Instalar os frameworks necessários

Crie uma `virtualenv` e nela instale os frameworks [Pypsa](https://github.com/PyPSA/PyPSA/) e a versão adaptada do [PADE](https://github.com/italocampos/pade/), este último, seguindo as instruções a seguir:

    * Clone o pade dentro da virtualenv;
    * Fora da pasta criada pelo clone, insira o comando sudo pip install -e pade;

### 2. Clonar o repositório do SMASH Python e ajuste os diretórios

Clone o repositório [pade-estudos](https://gitlab.com/laai/pade-estudos/) e dentro do diretório `smash-project`, crie a pasta `data` e dentro dela, as duas pastas `energy_files` e `tests`, ficando na seguinte estrutura:

        smash-project
        |_ data
            |_energy_files
            |_tests
 
### 3. Execute um arquivo de teste

Atualmente, o único arquivo testado é o `15-bus.py` baseado no modelo proposto pelo sistema [SMASH](https://gitlab.com/laai/smash/) original. Para executa-lo,
utilize o comando:

`pade start-runtime NOME_DA_TOPOLOGIA.py`

### Usar PySMASH com script

Dentro da pasta `smash-project` execute o comando `python script.py -file NOME_DA_TOPOLOGIA.py -agents AGENTES_AFETADOS`

Obs.: Os agentes afetados devem ser separados por vírgulas e sem espaço entre eles.


# Links interessantes

1. [Resultados dos testes](https://docs.google.com/spreadsheets/d/1IS9gC_ufKT5J_VtEhj5s2OulNEhgjJxivJx0w7HbQrs/edit?usp=sharing)
2. [Relação de erros identificados](https://docs.google.com/spreadsheets/d/1HYSRxH5xbvlEFn5VDI8YsB-n8g58twJQAdJOUUCTEas/edit?usp=sharing)
3. [Documentação do PADE](https://github.com/italocampos/pade/tree/master/docs/user/new-pade-docs)
4. [Documentação do PyPSA](https://pypsa.readthedocs.io/en/latest/)