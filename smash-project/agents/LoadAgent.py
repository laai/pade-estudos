import pickle
import traceback
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.misc.utility import display_message, start_loop
from behaviours.load.MappingManager import MappingManager
from behaviours.load.EnergyInspector import EnergyInspector
from behaviours.load.CreatePowerFile import CreatePowerFile
from behaviours.load.AttributesUpdater import AttributesUpdater
from behaviours.load.ConnectionManager import ConnectionManager
from behaviours.general.SubscribeInNetwork import SubscribeInNetwork

class LoadAgent(Agent):

	def __init__(self,aid,kargs):
		super().__init__(aid)
		self.__id = 0
		self.__supply = True
		self.__p_set = 0
		self.__q_set = 0
		self.__voltage = 0.0
		self.__neighbors = []
		self.__generator = None
		# Atributos do Sistema
		self.__requests = 0
		self.__time = 0,
		self.kargs = kargs # Recebe temporariamente os argumentos
		self.POWER_PATH = 'data/energy_files/' 
		
		# Agentes de Serviço
		self.SHARING_AREA = None
		self.NETWORK_AGENT = None

	def setup(self):

		self.startValues(self.kargs)

		# Cria o arquivo de energia do agente
		self.add_behaviour(CreatePowerFile(self))
		self.add_behaviour(EnergyInspector(self, 5))

		# Monitoramento e reconhecimento de vizinhos
		self.add_behaviour(ConnectionManager(self))

		# Gerenciador do mapeamento de rede elétrica
		self.add_behaviour(MappingManager(self))

		# Gerenciador de atualização de atributos locais
		self.add_behaviour(AttributesUpdater(self))

	def startValues(self, kargs):

		if (kargs.items() != None):
			try:
				self.setIdNode(kargs['id'])
				self.SHARING_AREA = kargs['sharing']
				self.NETWORK_AGENT = kargs['network']
				self.setP(kargs['p_set']) # Consumo ativo de energia (p_set)
				self.setQ(kargs['q_set']) # Consumo de energia reativa (q_set)
				self.setVoltage(kargs['voltage'])
				self.setGeneratorInfo(kargs['generator'])
				self.setEnergyStatus(True)
				self.resetRequests()

				self.add_behaviour(SubscribeInNetwork(self)) # Este comportamento serve para o agente se inscrever na rede criada pelo newtork agent.

			except Exception as e:
				display_message(self.aid.localname, "Some problem ocurred when I tried to read the input arguments.")
				print("Info: ")
				traceback.print_exc()
		else:
			display_message(self.aid.localname, "The passed arguments are incorrect. This agent will startup with default values.")
			self.setIdNode(0)
			self.setVoltage(0)
			self.setEnergyStatus(True)
			self.setP(0)
			self.setQ(0)
			self.resetRequests()
	
	@property
	def numNeighbors(self):
		return len(self.__neighbors)
		
	def connectNeighbor(self, neighbor): 
		self.__neighbors.append(neighbor)

	def connectNeighbors(self, neighbors):
		for neighbor in neighbors:
			self.connectNeighbor(neighbor)

	def getNeighbor(self, search):
		# Se for str a busca será por nome
		if isinstance(search, AID):
			for neighbor in self.__neighbors:
				if search == neighbor['agent']['name']:
					return neighbor
				return None
		# Se for int a busca será por índice
		elif isinstance(search, int):
			for i in range(len(self.__neighbors)):
				if search == i:
					return self.__neighbors[i]
				return None

	@property
	def getAllNeighbors(self):
		return self.__neighbors

	def whoMyNeighbor(self, switch):
		if not switch['node_1'] == self.aid:
			return switch['node_1']
		else:
			return switch['node_2']

	def disconnectNeighbor(self, name):
		for neighbor in self.__neighbors:
			if name == neighbor['agent']['name']:
				self.__neighbors.remove(neighbor)
				return True
			return False

	def disconecctAllNeighbors(self):
		self.__neighbors = []

	def setIdNode(self, id_node):
		if id_node  >= 0:
			self.__id = id_node

	def getIdNode(self):
		return self.__id

	def setEnergyStatus(self, status):
		self.__supply = status

	def hasEnergySupply(self):
		return self.__supply

	def setSupply(self, supply):
		self.__supply = supply

	def setQ(self, q):
		self.__q_set = q

	def setP(self, p):
		self.__p_set = p

	def getP(self):
		return self.__p_set

	def getQ(self):
		return self.__q_set

	def setVoltage(self, voltage):
		self.__voltage = voltage

	def getVoltage(self):
		return self.__voltage

	def setGeneratorInfo(self, info):
		self.__generator = info

	def incRequest(self):
		self.__requests = self.__requests + 1

	def decRequest(self):
		if self.__requests > 0:
			self.__requests = self.__requests - 1

	def hasRequests(self):
		return self.__requests > 0

	def getNumRequests(self):
		return self.__requests

	def resetRequests(self):
		self.__requests = 0

	def getDemand(self):
		return (self.__p_set, self.__q_set)

	# MEASURING EXECUTION TIME
	def setTime(self, time):
		self.__time = time 

	# MEASURING EXECUTION TIME
	def getTime(self):
		return self.__time

	# MEASURING EXECUTION TIME
	def getDiffTime(self, end):
		if end > self.__time:
			return end - self.__time
		return -1
		
	def isExternalAgent(self):
		if not self.hasEnergySupply():
			for neighbor in self.getAllNeighbors:
				# TODO: Enviar uma mensagem para o switch
				if neighbor['agent']['supply'] == True and neighbor['switch']['state'] == False:
					return True
				return False

	# Esta função serve para se encontrar o switch que une o agente ao seu vizinho
	def findSwitch(self, name):
		for connection in self.getAllNeighbors:
			if name == connection['agent']['name']:
				return connection['switch']['name']

	@property
	def status(self):
		status = {
			'id': self.__id,
			'name': self.aid,
			'demand': (self.__p_set, self.__q_set),
			'p_set': self.__p_set,
			'q_set': self.__q_set,
			'supply': self.__supply,
			'voltage': self.__voltage,
			'generator': self.__generator
		}

		message_status = pickle.dumps(status)

		return message_status

	def updateNeighbor(self, status):
		if self.disconnectNeighbor(status['agent']['name']):
			self.connectNeighbor(status)
			return True
		return False

	def __str__(self):

		if self.isExternalAgent():
			external = "YES" 
		else:
			external = "NO"
		
		if self.hasEnergySupply():
			on = "NORMAL"
		else:
			on = "INTERRUPTED"
		

		content = """\
AGENTE DE CARGA ************************
Nome do agente: {}
Identificador: {}
Gerador: {}
Agente Externo: {}
Abastecimento: {}
Tensão Elétrica: {} V
Demanda Elétrica: 
	Consumo ativo de energia (p_set): {} MW
	Consumo de energia reativa (q_set): {} MVar
----------------------------------------
Vizinhos: {}
****************************************\n""".format(self.aid.getName(), self.__id, self.isExternalAgent(), self.hasEnergySupply(), self.getVoltage(),self.getP, self.getQ, [x['agent']['name'] for x in self.__neighbors])
		return content