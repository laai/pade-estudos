import os
import time
import pypsa
import traceback
import numpy as np
import pandas as pd
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.misc.utility import display_message
from behaviours.network.TielinesManager import TielinesManager
from behaviours.network.SubscriptionManager import SubscriptionManager

# Este agente ficará responsável pela parte da interação com o PyPSA.
# Ele deve receber o cadastro de todos os agentes para montar a rede
# elétrica e fazer o cálculo para as possíveis configurações

class NetworkAgent(Agent):

    def __init__(self, aid):
        super().__init__(aid)
        self.__network = None
        self.loadbus_data = []
        self.line_data = []
        self.network_path = None

    def setup(self):
        self.add_behaviour(SubscriptionManager(self))
        self.add_behaviour(TielinesManager(self))

    # Esta função é apenas para auxiliar na ordenação das listas de componentes da rede
    def aux(self, data):
        return data['name'].localname

    def add_load_bus(self):
        # Organiza por nome a lista de loads para adicionar
        self.loadbus_data.sort(key=self.aux)
        for item in self.loadbus_data:
            self.__network.add("Bus", item['name'].localname, v_nom=item['voltage'])
            if item['generator']:
                self.__network.add("Generator", item['name'].localname, bus=item['name'].localname, control="PQ")
            else:
                # Nome, Bus, Demanda
                self.__network.add("Load", name=item['name'].localname, bus=item['name'].localname, p_set=item['p_set'], q_set=item['q_set'])

    def add_lines(self):
        self.line_data.sort(key=self.aux)  # Organiza por nome a lista de switchs para adicionar
        for item in self.line_data:
            self.__network.add("Line", item['name'].localname, bus0=item['node_1'].localname, bus1=item['node_2'].localname,
                                   r=item['resistance'], x=item['reactance'], length=item['lenght'])

    def add_tieline(self, data):
        self.add_components()
        self.__network.add("Line", data['name'].localname, bus0=data['node_1'].localname, bus1=data['node_2'].localname,
                               r=data['resistance'], x=data['reactance'], length=data['lenght'])
        
    def add_components(self):
        self.__network = pypsa.Network()
        self.add_load_bus()
        self.add_lines()
        
    def remove_line(self, line_name):
        for line in self.line_data:
            if line['name'].localname == line_name:
                self.line_data.remove(line)
                display_message(self.aid.localname, "Removed line in fault: {}".format(line['name'].localname))        

    def remove_bus_load(self, bus_load_name):
        self.__network.remove("Bus", bus_load_name)
        self.__network.remove("Load", bus_load_name)

    def get_eletrical_losses(self):
        self.__network.pf()  # Executa o cálculo de fluxo de potência
        power_losses = self.__network.lines_t.p0 + self.__network.lines_t.p1 # Valor de perda
        df = pd.DataFrame(data=power_losses) # Transforma as perdas da linha em DataFrame
        loss = df.sum(axis=1) # Soma as perdas da rede
        result = np.array(loss) # Conversão necessária para evitar erros de índice do Pandas
        return result[0]

    def print_network(self):
        self.__network.pf()
        print('\nLINHAS:\n{}'.format(self.__network.lines))
        print('\nLOADS:\n{}'.format(self.__network.loads))
        
    # Exporta os dados para CSV. Esta método é importante pois ele será utilizado posteriormente
    # pelo comportamento SetAttributes, que buscará pelo arquivo gerado aqui.     
    def export_csv(self, tag):
        self.__network.pf()
        data_path = 'data/tests/'
        self.network_path = os.path.join(data_path, tag)
        if not os.path.isdir(self.network_path):
            os.makedirs(self.network_path)
        self.__network.export_to_csv_folder(self.network_path)
        
    def print_lines(self):
        print('{}'.format(self.__network.lines))

    def print_busloads(self):
        print(self.__network.buses)
        print(self.__network.loads)

    def reset_data(self):
        self.loadbus_data = []
        self.line_data = []