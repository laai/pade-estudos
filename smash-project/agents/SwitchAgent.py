import traceback
import pickle
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from behaviours.sw.StatusSender import StatusSender
from behaviours.sw.ConnectNodes import ConnectNodes
from pade.behaviours.types import SequentialBehaviour
from behaviours.sw.SwitchStateManager import SwitchStateManager
from behaviours.general.SubscribeInNetwork import SubscribeInNetwork

class SwitchAgent(Agent):

	def __init__(self,aid,kargs):
		super().__init__(aid)
		self.__state = False
		self.__resistance = 0
		self.__reactance = 0
		self.__imax = 0
		self.__nodes = []
		self.kargs = kargs # Usado temporariamente para receber os argumentos
		self.lenght = 1 
		self.NETWORK_AGENT = None

	def setup(self):

		self.startValues(self.kargs)
		self.add_behaviour(SwitchStateManager(self))
		self.add_behaviour(StatusSender(self))

	def startValues(self, kargs):

		if (kargs != None):

			try:
				self.NETWORK_AGENT = kargs['network']
				# Se for uma linha ativa, ela esecutará esta ação
				if kargs['status']:
					sequential = SequentialBehaviour(self)
					sequential.add_subbehaviour(ConnectNodes(self, kargs['agent_1'], kargs['agent_2']))
					sequential.add_subbehaviour(SubscribeInNetwork(self))
					self.add_behaviour(sequential)
				# Se for uma tie line, esta executará esse else
				else:
					self.add_behaviour(ConnectNodes(self, kargs['agent_1'], kargs['agent_2']))

				self.setResistance(kargs['resistance'])
				self.setReactance(kargs['reactance'])
				self.setSwitchStatus(kargs['status'])
				self.setIMax(300)
				
			except Exception as e:
				display_message(self.aid.localname, "Problem reading the startup arguments.")
				print("Info:")
				traceback.print_exc()
		else:
			display_message(self.aid.localname, "Invalid parameters.")

	def connectNode(self, load):
		if len(self.__nodes) < 2:
			if not self.getNode(load):
				self.__nodes.append(load)
		else:
			display_message(self.aid.localname, "There are two agents already connected.")

	def getNode(self, search):
		if isinstance(search, AID):
			for agent in self.__nodes:
				if search == agent:
					return agent
				else:
					return None
		if isinstance(search, int):
			return self.__nodes[search]

	@property
	def getAllNodes(self):
		return self.__nodes

	@property
	def firstNode(self):
		return self.__nodes[0]

	@property
	def lastNode(self):
		return self.__nodes[1]

	def disconnectLoadAgents(self, name):
		for agent in self.__nodes:
			if name == agent:
				self.__nodes.remove(agent)
				return True
		return False

	def disconnectAllLoadAgents(self):
		self.__nodes = []

	def setSwitchStatus(self, status):
		if isinstance(status, bool):
			self.__state = status

	def openSwitch(self):
		self.__state = False

	def closeSwitch(self):
		self.__state = True

	def getSwitchState(self):
		return self.__state

	def isSwitchClosed(self):
		return self.__state

	def setIMax(self, imax):
		self.__imax = imax

	def getIMax(self):
		return self.__imax

	def setResistance(self, resistance):
		self.__resistance = resistance

	def setReactance(self, reactance):
		self.__reactance = reactance

	def getResistance(self):
		return self.__resistance

	def getReactance(self):
		return self.__reactance

	def numNodes(self):
		return len(self.__nodes)

	@property
	def status(self):
		status = {
			'name': self.aid,
			'nickname': (self.__nodes[0], self.__nodes[1]),
			'state': self.__state,
			'imax': self.__imax,
			'resistance': self.__resistance,
			'reactance': self.__reactance,
			'node_1': self.__nodes[0],
			'node_2': self.__nodes[1],
			'lenght': self.lenght
		}
		message_status = pickle.dumps(status)
		return message_status

	@property
	def state(self):
		state = {
			'name': self.aid,
			'state': self.__state,
			'imax': self.__imax,
			'resistance': self.__resistance,
			'lenght': self.lenght
		}
		message_state = pickle.dumps(state)
		return message_state		
	

	def __str__(self):	

		content = """\
	AGENTE DE CHAVE ************************
	Nome do agente: {}
	Estado da chave: {}
	Corrente Máxima: {}
	Resistência: {} Ohm
	Reatância: {} Ohm
	----------------------------------------
	Agentes conectados: {}
	****************************************\n""".format(self.aid.getLocalName(), self.__state, self.__imax, self.__resistance, self.__reactance, self.getResistance().real, [x for x in self.__nodes])
		return content