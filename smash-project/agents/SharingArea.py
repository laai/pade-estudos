from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.misc.utility import display_message
from behaviours.sharing.RegisterManager import RegisterManager

class SharingArea(Agent):

	def __init__(self, aid):
		super().__init__(aid)
		self.__current_active_agent = None
		self.__allAgents = []

	def setup(self):
		self.add_behaviour(RegisterManager(self))

	def registerActiveAgent(self, agent):
		self.__current_active_agent = agent

	def deregisterActiveAgent(self):
		self.__current_active_agent = None

	def existsActiveAgent(self):
		return self.__current_active_agent

	@property
	def activeAgentName(self):
		return self.__current_active_agent

	def isActiveAgent(self, agent):
		return self.__current_active_agent == agent

	def __str__(self):
		content = """\
	ÁREA DE COMPARTILHAMENTO ************************
	Nome do agente: {}
	Agente ativo atual {}
	****************************************\n""".format(self.aid.getLocalName(), self.__current_active_agent)
		return content