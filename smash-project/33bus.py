from pade.acl.aid import AID
from pade.misc.utility import start_loop
from agents.SwitchAgent import SwitchAgent
from agents.LoadAgent import LoadAgent 
from agents.SharingArea import SharingArea
from agents.NetworkAgent import NetworkAgent

if __name__ == '__main__':

	sharing_agent_aid = AID('sharing_agent')
	network_agent_aid = AID('network_agent')

	sharing_agent = SharingArea(sharing_agent_aid)
	network_agent = NetworkAgent(network_agent_aid)

	la_0_aid = AID('la_0')
	la_1_aid = AID('la_1')
	la_2_aid = AID('la_2')
	la_3_aid = AID('la_3')
	la_4_aid = AID('la_4')
	la_5_aid = AID('la_5')
	la_6_aid = AID('la_6')
	la_7_aid = AID('la_7')
	la_8_aid = AID('la_8')
	la_9_aid = AID('la_9')
	la_10_aid = AID('la_10')
	la_11_aid = AID('la_11')
	la_12_aid = AID('la_12')
	la_13_aid = AID('la_13')
	la_14_aid = AID('la_14')
	la_15_aid = AID('la_15')
	la_16_aid = AID('la_16')
	la_17_aid = AID('la_17')
	la_18_aid = AID('la_18')
	la_19_aid = AID('la_19')
	la_20_aid = AID('la_20')
	la_21_aid = AID('la_21')
	la_22_aid = AID('la_22')
	la_23_aid = AID('la_23')
	la_24_aid = AID('la_24')
	la_25_aid = AID('la_25')
	la_26_aid = AID('la_26')
	la_27_aid = AID('la_27')
	la_28_aid = AID('la_28')
	la_29_aid = AID('la_29')
	la_30_aid = AID('la_30')
	la_31_aid = AID('la_31')
	la_32_aid = AID('la_32')

				# Nome do Agente, Id do Agente, Tensão, Consumo ativo de energia (p_set), Consumo de energia reativa (q_set)
	la_0 = LoadAgent(la_0_aid, {'id': 0, 'voltage': 12.66, 'p_set': 0, 'q_set': 0, 'generator': True, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_1 = LoadAgent(la_1_aid, {'id': 1, 'voltage': 12.62, 'p_set': 0.1, 'q_set': 0.06, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_2 = LoadAgent(la_2_aid, {'id': 2, 'voltage': 12.44, 'p_set': 0.09, 'q_set': 0.04, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_3 = LoadAgent(la_3_aid, {'id': 3, 'voltage': 12.35, 'p_set': 0.12, 'q_set': 0.08, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_4 = LoadAgent(la_4_aid, {'id': 4, 'voltage': 12.26, 'p_set': 0.06, 'q_set': 0.03, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_5 = LoadAgent(la_5_aid, {'id': 5, 'voltage': 12.02, 'p_set': 0.06, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_6 = LoadAgent(la_6_aid, {'id': 6, 'voltage': 11.98, 'p_set': 0.2, 'q_set': 0.1, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_7 = LoadAgent(la_7_aid, {'id': 7, 'voltage': 11.81, 'p_set': 0.2, 'q_set': 0.1, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_8 = LoadAgent(la_8_aid, {'id': 8, 'voltage': 11.73, 'p_set': 0.06, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_9 = LoadAgent(la_9_aid, {'id': 9, 'voltage': 11.65, 'p_set': 0.06, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_10 = LoadAgent(la_10_aid, {'id': 10, 'voltage': 11.64, 'p_set': 0.045, 'q_set': 0.03, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_11 = LoadAgent(la_11_aid, {'id': 11, 'voltage': 11.62, 'p_set': 0.06, 'q_set': 0.035, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_12 = LoadAgent(la_12_aid, {'id': 12, 'voltage': 11.54, 'p_set': 0.06, 'q_set': 0.035, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_13 = LoadAgent(la_13_aid, {'id': 13, 'voltage': 11.52, 'p_set': 0.12, 'q_set': 0.08, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_14 = LoadAgent(la_14_aid, {'id': 14, 'voltage': 11.5, 'p_set': 0.06, 'q_set': 0.01, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_15 = LoadAgent(la_15_aid, {'id': 15, 'voltage': 11.48, 'p_set': 0.06, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_16 = LoadAgent(la_16_aid, {'id': 16, 'voltage': 11.45, 'p_set': 0.06, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_17 = LoadAgent(la_17_aid, {'id': 17, 'voltage': 11.45, 'p_set': 0.09, 'q_set': 0.04, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_18 = LoadAgent(la_18_aid, {'id': 18, 'voltage': 12.61, 'p_set': 0.09, 'q_set': 0.04, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_19 = LoadAgent(la_19_aid, {'id': 19, 'voltage': 12.57, 'p_set': 0.09, 'q_set': 0.04, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_20 = LoadAgent(la_20_aid, {'id': 20, 'voltage': 12.56, 'p_set': 0.09, 'q_set': 0.04, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_21 = LoadAgent(la_21_aid, {'id': 21, 'voltage': 12.55, 'p_set': 0.09, 'q_set': 0.04, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_22 = LoadAgent(la_22_aid, {'id': 22, 'voltage': 12.39, 'p_set': 0.09, 'q_set': 0.05, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_23 = LoadAgent(la_23_aid, {'id': 23, 'voltage': 12.31, 'p_set': 0.42, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_24 = LoadAgent(la_24_aid, {'id': 24, 'voltage': 12.27, 'p_set': 0.42, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_25 = LoadAgent(la_25_aid, {'id': 25, 'voltage': 12.0, 'p_set': 0.06, 'q_set': 0.025, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_26 = LoadAgent(la_26_aid, {'id': 26, 'voltage': 11.97, 'p_set': 0.06, 'q_set': 0.025, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_27 = LoadAgent(la_27_aid, {'id': 27, 'voltage': 11.82, 'p_set': 0.06, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_28 = LoadAgent(la_28_aid, {'id': 28, 'voltage': 11.72, 'p_set': 0.12, 'q_set': 0.07, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_29 = LoadAgent(la_29_aid, {'id': 29, 'voltage': 11.67, 'p_set': 0.2, 'q_set': 0.6, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_30 = LoadAgent(la_30_aid, {'id': 30, 'voltage': 11.62, 'p_set': 0.15, 'q_set': 0.07, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_31 = LoadAgent(la_31_aid, {'id': 31, 'voltage': 11.61, 'p_set': 0.21, 'q_set': 0.1, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_32 = LoadAgent(la_32_aid, {'id': 32, 'voltage': 11.6, 'p_set': 0.06, 'q_set': 0.04, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})

					# Nome do Agente, Nome do Agente 1, Nome do Agente 2, Resistência da linha, Reatância da linha, Status (Ativo/Inativo)
	sw_0 = SwitchAgent('sw_0', {'agent_1': la_1_aid, 'agent_2': la_0_aid, 'resistance': 0.0922, 'reactance': 0.0477,  'status': True, 'network': network_agent_aid})
	sw_1 = SwitchAgent('sw_1', {'agent_1': la_1_aid, 'agent_2': la_2_aid, 'resistance': 0.493, 'reactance': 0.2511,  'status': True, 'network': network_agent_aid})
	sw_2 = SwitchAgent('sw_2', {'agent_1': la_2_aid, 'agent_2': la_3_aid, 'resistance': 0.366, 'reactance': 0.1864,  'status': True, 'network': network_agent_aid})
	sw_3 = SwitchAgent('sw_3', {'agent_1': la_3_aid, 'agent_2': la_4_aid, 'resistance': 0.3811, 'reactance': 0.1941,  'status': True, 'network': network_agent_aid})
	sw_4 = SwitchAgent('sw_4', {'agent_1': la_4_aid, 'agent_2': la_5_aid, 'resistance': 0.819, 'reactance': 0.707,  'status': True, 'network': network_agent_aid})
	sw_5 = SwitchAgent('sw_5', {'agent_1': la_5_aid, 'agent_2': la_6_aid, 'resistance': 0.1872, 'reactance': 0.6188,  'status': True, 'network': network_agent_aid})
	sw_6 = SwitchAgent('sw_6', {'agent_1': la_6_aid, 'agent_2': la_7_aid, 'resistance': 1.7114, 'reactance': 1.2351,  'status': True, 'network': network_agent_aid})
	sw_7 = SwitchAgent('sw_7', {'agent_1': la_7_aid, 'agent_2': la_8_aid, 'resistance': 1.03, 'reactance': 0.74,  'status': True, 'network': network_agent_aid})
	sw_8 = SwitchAgent('sw_8', {'agent_1': la_8_aid, 'agent_2': la_9_aid, 'resistance': 1.04, 'reactance': 0.74,  'status': True, 'network': network_agent_aid})
	sw_9 = SwitchAgent('sw_9', {'agent_1': la_9_aid, 'agent_2': la_10_aid, 'resistance': 0.1966, 'reactance': 0.065,  'status': True, 'network': network_agent_aid})
	sw_10 = SwitchAgent('sw_10', {'agent_1': la_10_aid, 'agent_2': la_11_aid, 'resistance': 0.3744, 'reactance': 0.1238,  'status': True, 'network': network_agent_aid})
	sw_11 = SwitchAgent('sw_11', {'agent_1': la_11_aid, 'agent_2': la_12_aid, 'resistance': 1.468, 'reactance': 1.155,  'status': True, 'network': network_agent_aid})
	sw_12 = SwitchAgent('sw_12', {'agent_1': la_12_aid, 'agent_2': la_13_aid, 'resistance': 0.5416, 'reactance': 0.7129,  'status': True, 'network': network_agent_aid})
	sw_13 = SwitchAgent('sw_13', {'agent_1': la_13_aid, 'agent_2': la_14_aid, 'resistance': 0.591, 'reactance': 0.5260,  'status': True, 'network': network_agent_aid})
	sw_14 = SwitchAgent('sw_14', {'agent_1': la_14_aid, 'agent_2': la_15_aid, 'resistance': 0.7463, 'reactance': 0.545,  'status': True, 'network': network_agent_aid})
	sw_15 = SwitchAgent('sw_15', {'agent_1': la_15_aid, 'agent_2': la_16_aid, 'resistance': 1.289, 'reactance': 1.721,  'status': True, 'network': network_agent_aid})
	sw_16 = SwitchAgent('sw_16', {'agent_1': la_16_aid, 'agent_2': la_17_aid, 'resistance': 0.732, 'reactance': 0.574,  'status': True, 'network': network_agent_aid})
	sw_17 = SwitchAgent('sw_17', {'agent_1': la_1_aid, 'agent_2': la_18_aid, 'resistance': 0.164, 'reactance': 0.1565,  'status': True, 'network': network_agent_aid})
	sw_18 = SwitchAgent('sw_18', {'agent_1': la_18_aid, 'agent_2': la_19_aid, 'resistance': 1.5042, 'reactance': 1.3554,  'status': True, 'network': network_agent_aid})
	sw_19 = SwitchAgent('sw_19', {'agent_1': la_19_aid, 'agent_2': la_20_aid, 'resistance': 0.4095, 'reactance': 0.4784,  'status': True, 'network': network_agent_aid})
	sw_20 = SwitchAgent('sw_20', {'agent_1': la_20_aid, 'agent_2': la_21_aid, 'resistance': 0.7089, 'reactance': 0.9373,  'status': True, 'network': network_agent_aid})
	sw_21 = SwitchAgent('sw_21', {'agent_1': la_2_aid, 'agent_2': la_22_aid, 'resistance': 0.4512, 'reactance': 0.3083,  'status': True, 'network': network_agent_aid})
	sw_22 = SwitchAgent('sw_22', {'agent_1': la_22_aid, 'agent_2': la_23_aid, 'resistance': 0.898, 'reactance': 0.7011,  'status': True, 'network': network_agent_aid})
	sw_23 = SwitchAgent('sw_23', {'agent_1': la_23_aid, 'agent_2': la_24_aid, 'resistance': 0.896, 'reactance': 0.7091,  'status': True, 'network': network_agent_aid})
	sw_24 = SwitchAgent('sw_24', {'agent_1': la_5_aid, 'agent_2': la_25_aid, 'resistance': 0.203, 'reactance': 0.1034,  'status': True, 'network': network_agent_aid})
	sw_25 = SwitchAgent('sw_25', {'agent_1': la_25_aid, 'agent_2': la_26_aid, 'resistance': 0.2842, 'reactance': 0.1447,  'status': True, 'network': network_agent_aid})
	sw_26 = SwitchAgent('sw_26', {'agent_1': la_26_aid, 'agent_2': la_27_aid, 'resistance': 1.059, 'reactance': 0.9337,  'status': True, 'network': network_agent_aid})
	sw_27 = SwitchAgent('sw_27', {'agent_1': la_27_aid, 'agent_2': la_28_aid, 'resistance': 0.8042, 'reactance': 0.7006,  'status': True, 'network': network_agent_aid})
	sw_28 = SwitchAgent('sw_28', {'agent_1': la_28_aid, 'agent_2': la_29_aid, 'resistance': 0.5075, 'reactance': 0.2585,  'status': True, 'network': network_agent_aid})
	sw_29 = SwitchAgent('sw_29', {'agent_1': la_29_aid, 'agent_2': la_30_aid, 'resistance': 0.9744, 'reactance': 0.963,  'status': True, 'network': network_agent_aid})
	sw_30 = SwitchAgent('sw_30', {'agent_1': la_30_aid, 'agent_2': la_31_aid, 'resistance': 0.3105, 'reactance': 0.3619,  'status': True, 'network': network_agent_aid})
	sw_31 = SwitchAgent('sw_31', {'agent_1': la_31_aid, 'agent_2': la_32_aid, 'resistance': 0.341, 'reactance': 0.5302,  'status': True, 'network': network_agent_aid})

	# Tielines (Status = False)
	sw_32 = SwitchAgent('sw_32', {'agent_1': la_7_aid, 'agent_2': la_20_aid, 'resistance': 2.0, 'reactance': 2.0,  'status': False, 'network': network_agent_aid})
	sw_33 = SwitchAgent('sw_33', {'agent_1': la_8_aid, 'agent_2': la_14_aid, 'resistance': 2.0, 'reactance': 2.0,  'status': False, 'network': network_agent_aid})
	sw_34 = SwitchAgent('sw_34', {'agent_1': la_11_aid, 'agent_2': la_21_aid, 'resistance': 2.0, 'reactance': 2.0,  'status': False, 'network': network_agent_aid})
	sw_35 = SwitchAgent('sw_35', {'agent_1': la_17_aid, 'agent_2': la_32_aid, 'resistance': 0.5, 'reactance': 0.5,  'status': False, 'network': network_agent_aid})
	sw_36 = SwitchAgent('sw_36', {'agent_1': la_24_aid, 'agent_2': la_28_aid, 'resistance': 0.5, 'reactance': 0.5,  'status': False, 'network': network_agent_aid})

	# Inicializa os agentes no PADE
	start_loop([

		sharing_agent, network_agent,

		la_0,la_1,la_2,la_3,la_4,la_5,la_6,la_7,
		la_8,la_9,la_10,la_11,la_12,la_13,la_14,
		la_15,la_16,la_17,la_18,la_19,la_20,la_21,
		la_22,la_23,la_24,la_25,la_26,la_27,la_28,
		la_29,la_30,la_31,la_32,

		sw_0,sw_1,sw_2,sw_3,sw_4,sw_5,
		sw_6,sw_7,sw_8, sw_9,sw_10,sw_11,
		sw_12,sw_13,sw_14,sw_15,sw_16,
		sw_17,sw_18,sw_19,sw_20,sw_21,
		sw_22,sw_23,sw_24,sw_25,sw_26,
		sw_27,sw_28,sw_29,sw_30,sw_31,
		sw_32,sw_33,sw_34,sw_35,sw_36

		])	