# make the code as Python 3 compatible as possible
from __future__ import print_function, division
import pypsa
import pandas as pd
import numpy as np
import os
from pathlib import Path

network = pypsa.Network()

#add three buses
n_buses = 3

for i in range(n_buses):
    network.add("Bus","My bus {}".format(i),
                v_nom=20.)

#print(network.buses)

#add three lines in a ring
for i in range(n_buses):
    network.add("Line","My line {}".format(i),
                bus0="My bus {}".format(i),
                bus1="My bus {}".format((i+1)%n_buses),
                x=0.1,
                r=0.01)

#print(network.lines)

#add a generator at bus 0
network.add("Generator","My gen",
            bus="My bus 0",
            p_set=100,
            control="PQ")

#print(network.generators)

#print(network.generators.p_set)

#add a load at bus 1
network.add("Load","My load",
            bus="My bus 1",
            p_set=100)

#print(network.loads)
#print(network.loads.p_set)

network.loads.q_set = 100

#Do a Newton-Raphson power flow
network.pf()

"""
print(network.lines_t.p0)
print(network.buses_t.v_ang*180/np.pi)
print(network.buses_t.v_mag_pu)
"""

"""
power_losses = network.lines_t.p0 + network.lines_t.p1

print(power_losses)

soma = 0

df = pd.DataFrame(data=power_losses)

print(df.values)

for i in df.values:
    for j in i:
        soma += j

print("Soma: ", soma)"""


power_losses = network.lines_t.p0 + network.lines_t.p1 # Valor de perda
df = pd.DataFrame(data=power_losses)
#print(df)
dado = df.sum(axis=1)

result = dado.to_numpy()

print(result[0]) 

#log_file = open('auto.py', 'r')
log_path = os.path.abspath('auto.py')
#log_file.close()

print(log_path)


#network.export_to_csv_folder('teste-powerflow')

#print(type(network.buses))
#print(network.buses)

#new_voltages = pd.read_csv('teste-powerflow/buses-v_mag_pu.csv', encoding='ISO-8859-1')

#new_voltages = new_voltages.drop('Unnamed: 0', axis=1)

#print(new_voltages.columns)

#print(new_voltages['My bus 1'][0])


"""for i in new_voltages.columns:
    print(str(i))"""


# print(type(network.buses))