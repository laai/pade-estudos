import os
import time
import argparse

parser = argparse.ArgumentParser(prog='smash')
parser.add_argument('-agents', required = True)
parser.add_argument('-file', required = True)
 
args = parser.parse_args()

path = '/home/lubien/smash-testes/pade-estudos/smash-project/data/energy_files/la_'

os.remove('nohup.out')
os.system('nohup pade start-runtime ' + args.file +' --username user --password user &')
os.system('echo Aguardando agentes se conectarem...')
time.sleep(30) # Espera os agentes se conectarem

agents_in_fault = (args.agents).split(',')

# Remove os agentes em falta
for agent in agents_in_fault:
  os.remove(path + agent + '.pwr')
  os.system('echo Arquivo la_' + agent + '.pwr removido.')

time.sleep(50) # Espera para a finalização do SMASH
os.system('echo Encerrando execução.')
os.system('pkill pade') # Apaga o processo anterior