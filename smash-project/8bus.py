from pade.acl.aid import AID
from pade.misc.utility import start_loop
from agents.SwitchAgent import SwitchAgent
from agents.LoadAgent import LoadAgent 
from agents.SharingArea import SharingArea
from agents.NetworkAgent import NetworkAgent

if __name__ == '__main__':

	sharing_agent_aid = AID('sharing_agent')
	network_agent_aid = AID('network_agent')

	sharing_agent = SharingArea(sharing_agent_aid)
	network_agent = NetworkAgent(network_agent_aid)

	la_0_aid = AID('la_0')
	la_1_aid = AID('la_1')
	la_2_aid = AID('la_2')
	la_3_aid = AID('la_3')
	la_4_aid = AID('la_4')
	la_5_aid = AID('la_5')
	la_6_aid = AID('la_6')
	la_7_aid = AID('la_7')

	la_0 = LoadAgent(la_0_aid, {'id': 0, 'voltage': 12.66, 'p_set': 0.1, 'q_set': 0.09, 'generator':True, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_1 = LoadAgent(la_1_aid, {'id': 1, 'voltage': 12.66, 'p_set': 0.09, 'q_set': 0.04, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_2 = LoadAgent(la_2_aid, {'id': 2, 'voltage': 12.0, 'p_set': 0.12, 'q_set': 0.08, 'generator': True, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_3 = LoadAgent(la_3_aid, {'id': 3, 'voltage': 12.66, 'p_set': 0.06, 'q_set': 0.03, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_4 = LoadAgent(la_4_aid, {'id': 4, 'voltage': 12.66, 'p_set': 0.06, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_5 = LoadAgent(la_5_aid, {'id': 5, 'voltage': 12.0, 'p_set': 0.2, 'q_set': 0.1, 'generator': True, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_6 = LoadAgent(la_6_aid, {'id': 6, 'voltage': 12.66, 'p_set': 0.2, 'q_set': 0.1, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_7 = LoadAgent(la_7_aid, {'id': 7, 'voltage': 12.0, 'p_set': 0.06, 'q_set': 0.02, 'generator': True, 'network': network_agent_aid, 'sharing': sharing_agent_aid})

	sw_0 = SwitchAgent('sw_0', {'agent_1': la_0_aid, 'agent_2': la_1_aid, 'resistance': 0.0922, 'reactance': 0.0477,  'status': True, 'network': network_agent_aid})
	sw_1 = SwitchAgent('sw_1', {'agent_1': la_1_aid, 'agent_2': la_3_aid, 'resistance': 0.0669, 'reactance': 0.0864,  'status': True, 'network': network_agent_aid})
	sw_2 = SwitchAgent('sw_2', {'agent_1': la_1_aid, 'agent_2': la_4_aid, 'resistance': 0.1811, 'reactance': 0.1941,  'status': True, 'network': network_agent_aid})
	sw_3 = SwitchAgent('sw_3', {'agent_1': la_4_aid, 'agent_2': la_6_aid, 'resistance': 1.1966, 'reactance': 1.155,  'status': True, 'network': network_agent_aid})
	
	# Tie lines
	sw_4 = SwitchAgent('sw_4', {'agent_1': la_1_aid, 'agent_2': la_2_aid, 'resistance': 0.1500, 'reactance': 0.1500,  'status': False, 'network': network_agent_aid})
	sw_5 = SwitchAgent('sw_5', {'agent_1': la_4_aid, 'agent_2': la_5_aid, 'resistance': 0.1500, 'reactance': 0.1500,  'status': False, 'network': network_agent_aid})
	sw_6 = SwitchAgent('sw_6', {'agent_1': la_4_aid, 'agent_2': la_7_aid, 'resistance': 0.819, 'reactance': 0.707,  'status': False, 'network': network_agent_aid})
		
	start_loop([

		sharing_agent, network_agent,

		la_0, la_1, la_2, la_3, la_4, la_5, la_6, la_7,

		sw_0,sw_1,sw_2,sw_3,sw_4,sw_5,sw_6

		])	