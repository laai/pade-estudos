from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.behaviours.types import CyclicBehaviour

class StatusSender(CyclicBehaviour):

	"""Este comportamento responde a mensagem enviada pelo comportamento MappingManager."""

	def action(self):

		state_request = Filter()
		state_request.set_protocol('state-request')
		state_request.set_performative(ACLMessage.REQUEST)

		message = self.read()

		if state_request.filter(message):
			
			reply = message.create_reply() 

			if message.content == 'sw-state':

				if self.agent.isSwitchClosed():
					reply.set_performative(ACLMessage.CONFIRM)
				else:
					reply.set_performative(ACLMessage.DISCONFIRM)
			else:

				if message.content == 'all':
					reply.set_content(self.agent.status)
					reply.set_performative(ACLMessage.INFORM)
			
			self.agent.send(reply)