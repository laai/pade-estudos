import time
import pickle
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import SimpleBehaviour

class ConnectNodes(SimpleBehaviour):

	"""Este comportamento tem por função estabelecer a conexão entre dois nós. É iniciado junto ao agente de chave."""

	def __init__(self, agent, node_a, node_b):
		super().__init__(agent)
		self.__requested = False
		self.__done = False
		self.__tag = 'connection@' + str(time.time()*1000.0)
		self.__node_a = node_a
		self.__node_b = node_b

	def action(self):

		if not self.__requested:
			self.send_request()

		connection = Filter()
		connection.set_protocol('connection')
		connection.set_conversation_id(self.__tag)
		connection.set_language('load')

		message = self.read()

		if connection.filter(message):

			if message.get_performative() == ACLMessage.AGREE:

				self.agent.connectNode(self.__node_a)
				self.agent.connectNode(self.__node_b)
				#display_message(self.agent, 'The agents {} and {}  were successfully connected.'.format(self.__node_a.getLocalName(), self.__node_b.getLocalName()))
			
			elif message.get_performative() == ACLMessage.REFUSE:
				display_message(self.agent, '### The agent {} refused the connection.'.format(pickle.loads(message.content).getLocalName()))
			
			elif message.get_performative() == ACLMessage.FAILURE:
				display_message(self.agent, '### One or more agents are unreachable.')
			
			else:
				display_message(self.agent, '## A problem ocurred while agents {} and {} were connecting.'.format(self.__node_a.getLocalName(), self.__node_b.getLocalName()))
			
			self.__done = True

	def send_request(self):
		request = ACLMessage(ACLMessage.PROPOSE)
		request.set_protocol('connection')
		request.set_language('switch')
		request.set_ontology('step-1')
		request.set_conversation_id(self.__tag)
		request.add_receiver(self.__node_a)
		request.add_reply_to(self.__node_b)
		request.set_content(self.agent.state) # State é um dicionário que não contém dados ainda dos nós
		self.agent.send(request)
		self.__requested = True

	def done(self):
		return self.__done