from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import CyclicBehaviour

class SwitchStateManager(CyclicBehaviour):

	"""Este comportamento gerencia o estado do agente de chave."""

	def action(self):
		change_swstate = Filter()
		change_swstate.set_performative(ACLMessage.REQUEST)
		change_swstate.set_protocol('set-switch-state')
		change_swstate.set_language('load')

		message = self.read()

		if change_swstate.filter(message):
			if message.content == 'open':
				if not self.agent.isSwitchClosed():
					display_message(self.agent, "Switch is already open.")
				else:
					self.agent.openSwitch()
					display_message(self.agent, "Switch opened by {}.".format(message.sender.localname))
			else:
				if message.content == 'close':
					if self.agent.isSwitchClosed():
						display_message(self.agent, "Switch is already closed")
					else:
						self.agent.closeSwitch()
						display_message(self.agent, "Switch closed by {}.".format(message.sender.localname))
				else:
					display_message(self.agent, "Comando desconhecido")