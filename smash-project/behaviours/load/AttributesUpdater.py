import traceback
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import CyclicBehaviour

class AttributesUpdater(CyclicBehaviour):

	""" Este comportamento é utilizado por todos os agentes de carga que receberem mensagens do
	agente ativo através do comportamento PowerRestore. Desta forma, sua função é receber e
	atualizar a nova tensão obtida após a tomada de decisão"""

	def action(self):
		attributes = Filter()
		attributes.set_performative(ACLMessage.PROPOSE)
		attributes.set_ontology('set-attributes')

		message = self.read()

		if attributes.filter(message):
			try:
				#display_message(self.agent, 'Atualizei minha tensão de {} para {}'.format(self.agent.getVoltage(),round(float(message.content),2)))
				self.agent.setVoltage(round(float(message.content),2))
			except Exception as e:
				display_message(self.agent, 'Bad LoadState object to suceed the update.')
				traceback.print_exc()