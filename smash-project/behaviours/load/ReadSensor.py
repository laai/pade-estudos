from pade.acl.aid import AID
from pade.behaviours.types import OneShotBehaviour
from pade.core.agent import Agent
from pade.misc.utility import display_message

class ReadSensor(OneShotBehaviour):

	"""Este comportamento é responsável por verificar se o arquivo de energia do agente de carga existe."""
	
	def action(self):
		try:
			file = open('{}.pwr'.format(self.agent.POWER_PATH + self.agent.aid.localname), 'r')
			self.agent.setSupply(True)
		except:
			display_message(self.agent, "O agente {} não possui energia".format(self.agent.aid.localname))
			self.agent.setSupply(False)