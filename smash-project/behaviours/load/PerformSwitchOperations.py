import time
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.messages import ACLMessage
from pade.behaviours.types import OneShotBehaviour

class PerformSwitchOperations(OneShotBehaviour):

	"""Este comportamento é utilizado para interagir com o agente de chave numa
	possível abertura ou fechamento de chave."""

	def __init__(self, agent, sw_to_open, sw_to_close):
		super().__init__(agent)
		self.__to_open = sw_to_open
		self.__to_close = sw_to_close

	def action(self):

		# Preparando a mensagem de abertura de chaves
		message = ACLMessage(ACLMessage.REQUEST)
		message.set_ontology('set-switch-state')
		message.set_language('load')
		message.set_content('open')

		if self.__to_open != None:
			for receiver in self.__to_open:
				message.add_receiver(receiver['name'])
			self.agent.send(message)

		# Preparando a mensagem de fechamento de chaves
		message.reset_receivers()
		message.set_content('close')

		if self.__to_close != None:
			for receiver in self.__to_close:
				message.add_receiver(receiver['name'])
			self.agent.send(message)

		# MEASURING EXECUTION TIME
		print('The current self-healing process was successfully completed. Elapsed time: {}ms'.format(self.agent.getDiffTime(int(time.time()*1000.0))))