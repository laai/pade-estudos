from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.misc.utility import display_message
from pade.behaviours.types import OneShotBehaviour
from behaviours.load.SetAttributes import SetAttributes
from behaviours.load.CreatePowerFile import CreatePowerFile

class PowerRestore(OneShotBehaviour):

	"""Este comportamento é utilizado para iniciar o comportamento SetAttributes para todos
	os agentes da rede e também por recriar os arquivos de energia dos agentes que haviam
	sido afetados pela falha."""

	def __init__(self, agent, agents, agents_data, path):
		super().__init__(agent)
		self.__nodes = agents
		self.__agents = agents_data
		self.__path = path

	def action(self):
		self.agent.add_behaviour(SetAttributes(self.agent, self.__agents, self.__path))
		for agent in self.__nodes:
			#self.agent.add_behaviour(CreatePowerFile(self.agent, self.__nodes))
			try:
				file = open('{}.pwr'.format(str(self.agent.POWER_PATH + agent[0].localname)), 'w+')
				file.write('This file means that  {}  has power suply.'.format(agent[0]))
				file.close()
				display_message(self.agent, 'Agent {} has now power suply.'.format(agent[0].localname))
			except Exception as e:
				print("### Error while creating the power file {}.pwr:".format(agent[0]))
				print(e)