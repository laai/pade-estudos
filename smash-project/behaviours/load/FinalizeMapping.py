import time
import pickle
import json
import traceback
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import SimpleBehaviour

class FinalizeMapping(SimpleBehaviour):

	"""Este comportamento retorna ao comportamento Mapping o mapeamento da área afetada.
	Sua função é verificar as respostas recebidas dos agentes de cargas atingidos e
	a partir delas definir a área afetada, devolvendo este mapa ao solicitante, o
	agente ATIVO."""

	def __init__(self, agent, reply_to_requester, tag, submap):
		super().__init__(agent)
		self.__reply_to_requester = reply_to_requester
		self.__tag = tag
		self.__map = []
		self.__map.append(pickle.loads(submap))
		self.__done = False

	def action(self):
		passive_mapping = Filter()
		passive_mapping.set_protocol('mapping')
		passive_mapping.set_language('passive')
		passive_mapping.set_conversation_id(self.__tag)

		message = self.read()

		if passive_mapping.filter(message):

			# Caso o agente fizer parte da área afetada: armazena os dados no mapa da rede
			if message.get_performative() == ACLMessage.ACCEPT_PROPOSAL:
				try:
					submap = pickle.loads(message.content)
					self.__map.extend(submap)
					#display_message(self.agent, self.__map)
					# Decrementa o número de requisições para os nós vizinhos
					self.agent.decRequest()
				except Exception as e:
					display_message(self.agent, '### Bad LineState object.')
					traceback.print_exc()

			elif message.get_performative() == ACLMessage.REJECT_PROPOSAL:
				self.agent.decRequest()
				display_message(self.agent, 'Tie line {} can\'t be used at this network configuration.'.format(self.agent.findSwitch(message.sender).localname))	

			elif message.get_performative() == ACLMessage.FAILURE:
				self.agent.decRequest()
				display_message(self.agent, 'Fault spot located at {}.'.format(self.agent.findSwitch(message.sender).localname))

			else:
				display_message(self.agent, '### Something went wrong during the mapping process. Message sent by {}'.format(message.sender.localname))

		# Quando não houver mais aguardo por respostas de requisição de mapeamento,
		# o mapeamento termina (condição de parada)
		if not self.agent.hasRequests():
			self.__reply_to_requester.set_performative(ACLMessage.ACCEPT_PROPOSAL)
			self.__reply_to_requester.set_content(pickle.dumps(self.__map))
			self.agent.send(self.__reply_to_requester)
			self.__done = True

	def done(self):
		return self.__done