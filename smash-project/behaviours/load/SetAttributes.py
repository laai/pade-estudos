import pandas as pd
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import OneShotBehaviour

class SetAttributes(OneShotBehaviour):

	"""Este comportamento é responsável por calcular a nova tensão de cada agente de carga e encaminha-las
	para que estes a adicionem em suas informações."""

	def __init__(self, agent, agent_data, path):
		super().__init__(agent)
		self.path = path
		self.agents = agent_data

	def action(self):

		new_voltages = pd.read_csv('{}/buses-v_mag_pu.csv'.format(str(self.path)), encoding='ISO-8859-1') # Resgata os dados de tensão
		new_voltages = new_voltages.drop('Unnamed: 0', axis=1) # Remove a primeira coluna que não tem valor para nós

		for agent in self.agents:
			if agent['name'] in new_voltages.columns:
				new_voltage = new_voltages[agent['name']][0] * agent['voltage'] # Calcula a nova voltagem
				message = ACLMessage(ACLMessage.PROPOSE)
				message.set_ontology('set-attributes')
				message.set_language('smash-sys')
				message.add_receiver(agent['name'])
				message.set_content(new_voltage)
				self.agent.send(message)