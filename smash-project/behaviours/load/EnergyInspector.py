import time
import traceback
from pade.acl.aid import AID
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from behaviours.load.ReadSensor import ReadSensor
from pade.behaviours.types import TickerBehaviour
from behaviours.load.Election import Election

class EnergyInspector(TickerBehaviour):

	"""Este comportamento é iniciado junto com o agente e tem por objetivo monitorar através de outro comportamento,
	o ReadSensor, se o agente de carga possui energia. Caso contrário, ele inicia o comportamento Election para
	tentar se registrar como agente ATIVO."""

	def __init__(self, agent, delay):
		super().__init__(agent, delay)
		self.__requested = False

	def on_tick(self):

		self.agent.add_behaviour(ReadSensor(self.agent))

		if not self.agent.hasEnergySupply():
			if not self.__requested:
				self.agent.add_behaviour(Election(self.agent))
				self.agent.setTime(int(time.time()*1000))
				self.__requested = True
		else:
			if self.__requested:
				display_message(self.agent, 'Power supply restored.')
				self.__requested = False