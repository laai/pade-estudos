import json
import time
import pickle
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import SimpleBehaviour
from behaviours.load.Deregister import Deregister
from behaviours.load.CloseSwitch import CloseSwitch
from behaviours.load.PowerRestore import PowerRestore

class RequestPowerFlow(SimpleBehaviour):

	"""Este comportamento tem por objetivo verificar os agentes recebidos no mapeamento e setar os dados
	para então envia-los ao agente de rede. Calculada a melhor configuração possível, o comportamento
	encerra solicitando os comportamentos PowerRestore, CloseSwitch e o Deregister. Recebe os dados do
	agente de rede enviados pelo comportamento SendPowerFlowResults."""

	def __init__(self, agent, mapping, fault):
		super().__init__(agent)
		self.__done = False
		self.__map = mapping
		self.__fault = fault
		self.__tag = 'powerflow@' + str(time.time()*1000.0)
		self.requested = False
		self.__tielines = []
		self.__buses = set()

	def action(self):

		if not self.requested:

			# Este for vai setar os dados dos bus, isto é, dos loads
			[self.__buses.add((bus['agent']['name'], bus['agent']['voltage'], bus['agent']['demand'], bus['agent']['generator'])) for bus in self.__map]

			# Detectando as tie lines no mapa da área afetada.
			for element in self.__map:
				# Se a chave não está fechada é a tie line
				if not element['switch']['state']:
					self.__tielines.append(element['switch'])

			if not self.__tielines:
				display_message(self.agent, 'There are no connection points to recover the affected area.')
			else:
				# Envia os dados para o PowerFlow (recebidos no comportamento TielinesManager)
				message = ACLMessage(ACLMessage.PROPOSE)
				message.set_ontology('add-tieline')
				message.set_language('smash-sys')
				message.set_conversation_id(self.__tag)
				message.add_receiver(self.agent.NETWORK_AGENT)
				message.set_content(pickle.dumps({'tielines': self.__tielines, 'sw': self.__fault }))
				self.agent.send(message)

				self.requested = True
		
		# Recebe mensagens apenas do comportamento SendPowerFlowResults
		powerflow_message = Filter()
		powerflow_message.set_performative(ACLMessage.INFORM)
		powerflow_message.set_conversation_id(self.__tag)
		powerflow_message.set_ontology('powerflow-result')
		powerflow_message.set_language('smash-sys')

		message_power = self.read()

		if powerflow_message.filter(message_power):

			network_response = pickle.loads(message_power.content)

			sw_powerflow = network_response['result']
			data_agents = network_response['agents']
			path_file = network_response['path']
			log_path = network_response['log_path']

			self.__buses.add((self.agent.aid, self.agent.getVoltage(), self.agent.getDemand()))

			self.agent.add_behaviour(CloseSwitch(self.agent, sw_powerflow, log_path))
			self.agent.add_behaviour(PowerRestore(self.agent, self.__buses, data_agents, path_file))
			self.agent.add_behaviour(Deregister(self.agent))
	
		self.__done = True

	def done(self):
		return self.__done