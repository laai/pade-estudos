import time
import traceback
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import SimpleBehaviour
from behaviours.load.Mapping import Mapping

class Election(SimpleBehaviour):

	"""Este comportamento é iniciado no comportamento EnergyInspector quando o agente percebe que não
	possui energia. Sua função então, é enviar uma mensagem ao sharing agent a fim de dentar se registrar
	como agente ATIVO."""

	def __init__(self, agent):
		super().__init__(agent)
		self.__tag = 'election@' + str(time.time()*1000.0)
		self.__done = False
		self.__requested = False

	def action(self):

		if not self.__requested:
			ticket = ACLMessage(ACLMessage.REQUEST)
			ticket.set_conversation_id(self.__tag)
			ticket.set_protocol('register')
			ticket.set_content('register')
			ticket.add_receiver(self.agent.SHARING_AREA)
			self.agent.send(ticket)
			self.__requested = True

		election = Filter()
		election.set_conversation_id(self.__tag)
		election.set_protocol('register')

		message = self.read()

		if election.filter(message):

			if message.get_performative() == ACLMessage.CONFIRM:
				self.agent.add_behaviour(Mapping(self.agent))
			elif message.get_performative() == ACLMessage.REFUSE:
				pass
			else:
				display_message(self.agent, '### Something went wrong, {} is misbehaving.'.format(self.agent.SHARING_AREA))
			self.__done = True

	def done(self):
		return self.__done