import time
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import OneShotBehaviour

class CloseSwitch(OneShotBehaviour):

	""" Este comportamento é utilizado pelo agente ATIVO em RequestPowerFlow para
	enviar uma mensagem ao agente de chave que deve ser fechado em definitivo. """ 

	def __init__(self,agent,sw_to_close,log_path):
		super().__init__(agent)
		self.__sw_to_close = sw_to_close
		self.healing_time = None
		self.log_path = log_path

	def action(self):
		message = ACLMessage(ACLMessage.REQUEST)
		message.add_receiver(self.__sw_to_close)
		message.set_protocol('set-switch-state')
		message.set_language('load')
		message.set_content('close')
		self.agent.send(message)
		self.healing_time = self.agent.getDiffTime(int(time.time()*1000))
		display_message(self.agent, 'The current self-healing process was successfully completed. Elapsed time: {}ms'.format(self.healing_time))
		
		file = open(self.log_path, 'r')
		conteudo = file.readlines()
		file.close()

		conteudo.append('HEALING PROCESS TIME: {}ms\n\n'.format(self.healing_time))
		arquivo = open(self.log_path, 'w')
		arquivo.writelines(conteudo)
		arquivo.close()