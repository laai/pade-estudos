import os
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.messages import ACLMessage
from pade.behaviours.types import OneShotBehaviour
class CreatePowerFile(OneShotBehaviour):

	"""Este comportamento é iniciado junto com os agentes de carga. Ele é responsável por criar
	os arquivos que indicam que este possui energia."""

	def action(self):
		try:
			file = open('{}.pwr'.format(str(self.agent.POWER_PATH + self.agent.aid.getLocalName())), 'w+')
			file.write('This file means that  {}  has power suply.'.format(self.agent.aid.getLocalName()))
			file.close()
		except Exception as e:
			print("### Error while creating the power file {}.pwr:".format(self.agent.aid.getLocalName()))
			print(e)
   