import time
import pickle
import traceback
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from behaviours.load.ReadSensor import ReadSensor
from pade.behaviours.types import CyclicBehaviour
from behaviours.load.FinalizeMapping import FinalizeMapping

class MappingManager(CyclicBehaviour):

    """Este comportamento tem como função gerenciar as mensagens de Mapeamento. Ele é
    instanciado junto a todos os agentes de carga e é iniciado pelo nó vizinho ao agente
    ATIVO a partir do recebimento de mensagem enviado pelo agente ATIVO no comportamento Mapping. 
    Após receber a mensagem, ele então salva uma resposta para enviar ao agente ATIVO posteriormente.
    Recebendo a resposta do agente de chave, ele inicia os procedimentos para solicitar ou não o
    estado de seus vizinhos que prosseguem com o mapeamento caso sejam detectada falhas. Ao final
    ele finaliza o mapeamento de seus vizinhos e retorna o resultado para FinalizeMapping."""

    def __init__(self, agent):
        super().__init__(agent)
        self.__requester = None
        self.__requester_state = None
        self.__reply = None
        self.__tag = ''
        self.__locked = False

    def action(self):
        
        self.agent.add_behaviour(ReadSensor(self.agent))

        # Resposta preparada para responder ao "nó pai", isso é, o nó solicitante
        if not self.__locked:

            mapping = Filter()
            mapping.set_performative(ACLMessage.REQUEST)
            mapping.set_protocol('mapping')
            mapping.set_language('passive')

            message = self.read()
            # Filtro para mensagem recebida do comportamento Mapping.py
            if mapping.filter(message):

                self.__reply = message.create_reply()
                self.__requester = message.sender
                # Responde ao requisitante com a linguagem passada no slot 'ontology'
                self.__reply.set_language(message.get_ontology())

                try:
                    # Salva os dados enviados pelo requisitante
                    self.__requester_state = pickle.loads(message.content)
                except Exception as e:
                    display_message(self.agent, '### Error while saving the requester state (Bad LoadState object).')
                    traceback.print_exc()

                # Requisita estado da linha entre este agente e o requisitante
                request = ACLMessage(ACLMessage.REQUEST)
                request.set_protocol('state-request')
                request.set_content('all')
                self.__tag = 'request@' + str(int(time.time()*1000.0))
                request.set_conversation_id(self.__tag)
                request.add_receiver(self.agent.findSwitch(self.__requester))
                self.agent.send(request)

                # Bloqueia o comportamento até receber a resposta do SwitchAgent
                self.__locked = True

        else:

            # Filtra mensagens de atualização de estado de linha (SwitchAgent)

            mapping2 = Filter()
            mapping2.set_performative(ACLMessage.INFORM)
            mapping2.set_protocol('state-request')
            mapping2.set_conversation_id(self.__tag)

            message = self.read()

            if mapping2.filter(message):

                try:
                    # Captura o estado da linha enviado pelo SwitchAgent
                    sw_state = pickle.loads(message.content)

                    # Se a chave está fechada
                    if sw_state['state'] == True:

                        """ Se o agente não tem energia e o status da chave é fechada, o mapeamento deve continuar e os dados precisam
                        ser enviados ao agente requisitante. """

                        if not self.agent.hasEnergySupply():
                            
                            # Captura os dados da linha entre o LoadAgent requisitante e este
                            line = { 'agent': pickle.loads(self.agent.status), 'switch': sw_state }                            
                            line = pickle.dumps(line)
                            
                            # Aguarda as respostas dos filhos para finalizar o mapeamento
                            self.__tag = 'mapping@' + str(int(time.time()*1000.0))
                            
                            self.agent.add_behaviour(FinalizeMapping(self.agent, self.__reply, self.__tag, line))

                            # Prepara requisições para os demais agentes vizinhos.
                            request = ACLMessage(ACLMessage.REQUEST)
                            request.set_protocol('mapping')
                            # Comunica-se com os demais LoadAgent usando uma linguagem "passive"
                            request.set_language('passive')
                            request.set_ontology('passive')
                            request.set_content(self.agent.status)
                            request.set_conversation_id(self.__tag)
                            # Adiciona-se os agentes vizinhos como remetentes
                            for neighbor in self.agent.getAllNeighbors:
                                # Exclui-se o nó que fez a requisição
                                if not neighbor['agent']['name'] == self.__requester:
                                    # display_message(self.agent, 'Enviando mensagem para meu vizinho {}'.format(neighbor['agent']['name'].localname))
                                    request.add_receiver(neighbor['agent']['name'])
                                    # Incrementa o número de requisições para os nós vizinhos
                                    self.agent.incRequest()
                            self.agent.send(request)

                        else:

                            """ Se a carga não está em falta, encontrou-se o ponto de falha. Este agente deve
                            abrir a chave da linha com falha e notificar ao agente requisitante que a área 
                            afetada termina aqui. """

                            # Requisita-se a abertura de chave da linha em falta
                            response = ACLMessage(ACLMessage.REQUEST)
                            response.set_protocol('set-switch-state')
                            response.set_language('load')
                            response.set_content('open')
                            #display_message(self.agent, self.agent.findSwitch(self.__requester))
                            response.add_receiver(self.agent.findSwitch(self.__requester))
                            self.agent.send(response)

                            # Notifica agente requisitante para prosseguir mapeamento sem considerar esta linha
                            self.__reply.set_performative(ACLMessage.FAILURE)
                            self.agent.send(self.__reply)

                    # Se a carga não está em falta e a chave não está fechada, este é um ponto de conexão, uma tie line
                    else:

                        if self.agent.hasEnergySupply():
                            
                            # Mapeia linha entre os dois agentes

                            """ 
                            Já que é uma tieline, precisa estar evidente qual o nó que possui energia para
                            ajudar no powerflow. Para isso, adiciono dois parâmetros no status do switch:
                            connection_voltage e connection_node que servirão para ser usados na escrita
                            do JSON com as possíveis configurações da rede.
                            """
                            connection_agent = pickle.loads(self.agent.status)
                            sw_state['connection_voltage'] = connection_agent['voltage']
                            sw_state['connection_node'] = connection_agent['name']
                            sw_state['connection_demand'] = connection_agent['demand']

                            line = { 'agent': self.__requester_state, 'switch': sw_state }
                            lines = []
                            lines.append(line)

                            # Responde ao requisitante sem continuar a busca na rede
                            self.__reply.set_performative(ACLMessage.ACCEPT_PROPOSAL)

                            # Responde ao agente requisitante com o submapa da rede
                            self.__reply.set_content(pickle.dumps(lines))
                            self.agent.send(self.__reply)
                            #display_message(self.agent, 'Esta linha pode usada, enviei meus dados para o requisitante ({}).'.format(self.__requester))
            
                        else:

                            # Se a carga está em falta, é uma tie line que não pode ser usada na autorrecuperação
                            # Notifica agente requisitante para prosseguir mapeamento sem considerar esta linha
                            self.__reply.set_performative(ACLMessage.REJECT_PROPOSAL)
                            self.agent.send(self.__reply)
                            #display_message(self.agent, 'Esta linha não pode ser usada')

                except Exception as e:
                    display_message(self.agent, '### Error while getting state from electric switch (Bad Switch/LineState object).')
                    traceback.print_exc()

                self.__locked = False