from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.messages import ACLMessage
from pade.behaviours.types import OneShotBehaviour

class Deregister(OneShotBehaviour):

	"""Este comportamento é utilizado pelo agente ATIVO quando este deseja enviar uma mensagem ao
	sharing agent a fim de remover seu registro como agente ATIVO. Ele é utilizado por ele em outro
	comportamento, o RequestPowerFlow"""

	def action(self):
		message = ACLMessage(ACLMessage.REQUEST)
		message.set_protocol('register')
		message.add_receiver(self.agent.SHARING_AREA)
		message.set_content('deregister')
		self.agent.send(message)