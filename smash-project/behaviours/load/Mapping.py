import pickle
import traceback
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import SimpleBehaviour
from behaviours.load.RequestPowerFlow import RequestPowerFlow

class Mapping(SimpleBehaviour):

	""" Comportamento executado pelo agente ativo. Ele inicia o mapeamento da
	área afetada e aguarda receber o mapa dos agentes vizinhos, para então 
	solicitar ao agente de rede através do comportamento RequestPowerFlow, o
	cálculo de fluxo de potência para cada configuração possível da rede."""

	def __init__(self, agent):
		super().__init__(agent)
		self.__done = False
		self.__started = False
		self.__fault = ''
		self.__map = []

	def action(self):

		if not self.__started:
			# Mensagem que será tratada no comportamento MappingManager.py
			# Prepara requisições para os demais agentes vizinhos.
			request = ACLMessage(ACLMessage.REQUEST)
			request.set_protocol('mapping')
			request.set_language('passive')
			request.set_ontology('active')
			request.set_content(self.agent.status)

			for neighbor in self.agent.getAllNeighbors:
				request.add_receiver(neighbor['agent']['name'])
				self.agent.incRequest() # Incrementa o número de requisições feitas para os nós vizinhos

			self.agent.send(request)
			self.__started = True

		message = self.read()

		mapping = Filter()
		mapping.set_protocol('mapping')
		mapping.set_language('active')

		if mapping.filter(message):

			# Caso o remetente faça parte da área afetada, ele deve salvar a o mapa recebido
			if message.get_performative() == ACLMessage.ACCEPT_PROPOSAL:
				self.agent.decRequest()
				try:
					submap = pickle.loads(message.content)
					#display_message(self.agent, submap) # APAGAR DEPOIS
					self.__map.extend(submap)
				except Exception as e:
					display_message(self.agent, '### Bad LineState object.')
					traceback.print_exc()
			
			#  Caso a tie line não possa ser usada na autorrecuperação
			elif message.get_performative() == ACLMessage.REJECT_PROPOSAL:
				self.agent.decRequest()
				display_message(self.agent, 'Tie line {} can\'t be used at this network configuration'.format(self.agent.getNeighbor(message.sender).localname))
			
			# Caso o agente emissor já tenha sido mapeado
			# Caso o remetente não faça parte da área afetada: é o ponto de falha
			elif message.get_performative() == ACLMessage.FAILURE:
				self.agent.decRequest()
				display_message(self.agent, 'Fault spot located at {}.'.format(self.agent.findSwitch(message.sender).localname))
				self.__fault = self.agent.findSwitch(message.sender).localname
			
			else:
				display_message(self.agent, '### Something went wrong during the mapping process.')

			if not self.agent.hasRequests():
				display_message(self.agent, 'The mapping proccess is ended.')
				setNodes = set()				
				for element in self.__map:
					setNodes.add((element['switch']['node_1'].localname, element['switch']['node_2'].localname))
				
				display_message(self.agent, 'Affected area {}'.format(list(setNodes)))

				# Este comportamento requisita o início do powerflow para cada configuração possível
				self.agent.add_behaviour(RequestPowerFlow(self.agent, self.__map, self.__fault))
				self.__done = True

	def done(self):
		return self.__done