from pade.acl.aid import AID
from pade.acl.messages import ACLMessage
from pade.behaviours.types import CyclicBehaviour
from pade.core.agent import Agent
from pade.misc.utility import display_message
from pade.acl.filters import Filter
from behaviours.load.NewConnection import NewConnection
import pickle
import traceback

class ConnectionManager(CyclicBehaviour):

	"""Este comportamento é adicionado para cada agente de chave, de forma a gerenciar a solicitação
	de novas conexões, podendo receber mensagens tanto do agente de chave, quanto de outro agente de
	carga, agindo de acordo com filtros pré-estabelecidos."""

	def action(self):

		# Filtros das possíveis mensagens
		connection = Filter()
		connection.set_performative(ACLMessage.PROPOSE)
		connection.set_ontology('step-1')
		connection.set_protocol('connection')

		message = self.read()
				
		if connection.filter(message):
			# Recebe mensagem enviada pelo agente Switch em ConnectNode para estabelecer uma conexão entre dos agentes
			if message.get_language() == 'switch':
				try:
					switch_data = pickle.loads(message.content) # Obtem o status do agente de chave
					agent_data = pickle.loads(self.agent.status) # Recupera seu próprio status
					request = message.create_reply() # Prepara resposta para o agente de chave
					data = { 'agent': agent_data, 'switch': switch_data } 
					request.set_language('load')
					request.set_content(pickle.dumps(data))
					self.agent.add_behaviour(NewConnection(self.agent, switch_data, message.get_conversation_id()))
					self.agent.send(request)
				except Exception as e:
					display_message(self.agent, '### Bad SwitchStatus object.')
					traceback.print_exc()
			else:
				# Recebe mensagem enviada por outro agente Load para consolidar a conexão
				if message.get_language() == 'load':
					try:
						neighbor = pickle.loads(message.content)
						self.agent.connectNeighbor(neighbor)
						reply = message.create_reply()
						reply.set_content(self.agent.status)
						reply.set_performative(ACLMessage.AGREE)
						reply.set_ontology('step-2')
						self.agent.send(reply)
					except Exception as e:
						display_message(self.agent, '### Bad Neighbor object')
						traceback.print_exc()