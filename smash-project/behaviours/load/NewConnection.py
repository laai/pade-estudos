from pade.acl.aid import AID
from pade.acl.messages import ACLMessage
from pade.behaviours.types import SimpleBehaviour
from pade.core.agent import Agent
from pade.misc.utility import display_message
from pade.acl.filters import Filter
import pickle
import traceback

class NewConnection(SimpleBehaviour):

	"""Este comportamento é inicaido em ConnectionManager quando se deseja estabelecer a conexão.
	Adiciona o novo vizinho e interage com o agente de chave."""

	def __init__(self, agent, sw_data, conversation_id):
		super().__init__(agent)
		self.__switch_data = sw_data
		self.__tag = conversation_id
		self.__done = False

	def action(self):
		connection = Filter()
		connection.set_protocol('connection')
		connection.set_language('load')
		connection.set_ontology('step-2')
		connection.set_conversation_id(self.__tag)

		message = self.read()

		# Mensagem enviada pelo agente Load aceitando estabelecer a conexão

		if connection.filter(message):

			if message.get_performative() == ACLMessage.AGREE:
				try:
					neighbor = pickle.loads(message.content)
					neighbor_sw = { 'agent': neighbor, 'switch': self.__switch_data }
					self.agent.connectNeighbor(neighbor_sw) # Adiciona as informações do agente e do switch que os conectará
					self.notifySwitchAgent(ACLMessage.AGREE, neighbor['name'])
				except Exception as e:
					display_message(self.agent, '### Bad Load object.')
					traceback.print_exc()

			elif message.get_performative() == ACLMessage.REFUSE:
				display_message(self.agent, '### Agent {} refused the connection.'.format(neighbor['name']))
				self.notifySwitchAgent(ACLMessage.REFUSE, neighbor['name'])

			else:
				display_message(self.agent, '### It seems that {} is not responding. This agent wont init.'.format(message.sender.localname))
				self.notifySwitchAgent(ACLMessage.UNKNOWN, '')

			self.__done = True

	def notifySwitchAgent(self, performative, agent_name):
		notification = ACLMessage(performative)
		notification.set_conversation_id(self.__tag)
		notification.set_protocol('connection')
		notification.set_language('load')
		notification.add_receiver(self.__switch_data['name'])
		notification.set_content(pickle.dumps(agent_name))
		self.agent.send(notification)

	def done(self):
		return self.__done