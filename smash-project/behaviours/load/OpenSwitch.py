from pade.acl.aid import AID
from pade.acl.messages import ACLMessage
from pade.behaviours.types import OneShotBehaviour
from pade.core.agent import Agent

class OpenSwitch(OneShotBehaviour):

	"""Este comportamento serve para solicitar ao agente de chave, a abertura desta.
	Utilizado pelo agente ATIVO no comportamento """

	def __init__(self, agent, sw_to_open):
		super().__init__(agent)
		self.__sw_to_open = sw_to_open

	def action(self):
		# Preparando a mensagem de abertura de chaves
		message = ACLMessage(ACLMessage.REQUEST)
		message.set_ontology('change-swstate')
		message.set_language('smash-sys')
		message.add_receiver(self.__sw_to_open['name'])
		message.set_content('open')
		self.agent.send(message)