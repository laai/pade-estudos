import pickle
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.messages import ACLMessage
from pade.behaviours.types import OneShotBehaviour

class SubscribeInNetwork(OneShotBehaviour):

	def __init__(self, agent):
		super().__init__(agent)

	def action(self):
		message = ACLMessage(ACLMessage.PROPOSE)
		message.set_ontology('subscribe')
		message.set_language('smash-sys')
		message.add_receiver(self.agent.NETWORK_AGENT)
		message.set_content(self.agent.status)
		self.agent.send(message)