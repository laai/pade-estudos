# Recebe como dado um dicionário com as chaves 'tieline' e 'sw'
# (este último é a chave que apresentou problema e deve ser removida) 
import os
import pickle
import traceback
from pathlib import Path
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import CyclicBehaviour
from behaviours.network.SendPowerFlowResults import SendPowerFlowResults

class TielinesManager(CyclicBehaviour):

	"""Este comportamento é responsável por aplicar o fluxo de potência a cada configuração possível e retornar
	ao agente ATIVO através do comportamento SendPowerFlowResults, os resultados da melhor configuração obtida,
	bem como os agentes de carga que devem se atualizar."""

	def __init__(self, agent):
		super().__init__(agent)
		self.__receiver = None
		self.menor = ''
		self.__result = dict()
		self.__tag = ''
		self.fault = None

	def action(self):

		# Filtros das possíveis mensagens (responde ao comportamento PerformPowerFlow)
		tieline_acceptor = Filter()
		tieline_acceptor.set_performative(ACLMessage.PROPOSE)
		tieline_acceptor.set_ontology('add-tieline')
		tieline_acceptor.set_language('smash-sys')

		message = self.read()

		self.__tag = message.get_conversation_id()
		self.__receiver = message.sender
		
		if tieline_acceptor.filter(message):
			data = pickle.loads(message.content)

			display_message(self.agent, 'Powerflow process tag: {}'.format(self.__tag))
			self.fault = data['sw']

			try:
				self.agent.remove_line(self.fault)
				for tieline in data['tielines']:
					self.agent.add_components()
					self.agent.add_tieline(tieline) # Adiciona a tieline
					self.__result[tieline['name'].localname] = self.agent.get_eletrical_losses() # Registra as perdas para esta linha
			
				self.losses_table()
				self.agent.add_tieline(self.min_power_loss_tieline(data['tielines']))
				self.agent.line_data.append(self.min_power_loss_tieline(data['tielines']))
				self.agent.export_csv(self.__tag)
				
				# Cria um relatório simples com os dados desta recuperação
				log_file_name = '{}/healing_process.log'.format(self.agent.network_path)
				log_content = 'AGENTE ATIVO: {} \nPOWERFLOW TAG: {}\nLINHA EM FALTA: {}\nCHAVE FECHADA: {}\nTIELINES & PERDAS: {}\n'.format(message.sender.localname, self.__tag, self.fault, self.menor, self.__result)
				log_file = open(log_file_name, 'w+')
				log_file.write(log_content)
				log_file.close()
				log_path = os.path.abspath(log_file_name)

				self.__result = dict()
    
				self.agent.add_behaviour(SendPowerFlowResults(self.agent, self.menor, self.__receiver, self.__tag, self.agent.loadbus_data, self.agent.network_path, log_path))

			except Exception as e:
				display_message(self.agent, '### Bad Tieline object.')
				traceback.print_exc()
    
	def losses_table(self):
		print("+=============================================+\n POWER LOSSES\n+=============================================+")
		[print(" {} : {} \t".format(i, j)) for i, j in self.__result.items()]
		print("+=============================================+")
		self.menor = min(self.__result, key=self.__result.get)
		print(" Menor perda em {} com o valor {}".format(self.menor, round(float(self.__result[self.menor]), 9)))
		print("+=============================================+")

	def min_power_loss_tieline(self, tielines):
		for tieline in tielines:
			if tieline['name'].localname == self.menor:
				return tieline