import pickle
from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import OneShotBehaviour

class SendPowerFlowResults(OneShotBehaviour):

    """Este comportamento tem por função enviar ao agente ATIVO os dados obtidos da
    melhor configuração obtida no cálculo de fluxo de potência."""
    
    def __init__(self, agent, result, receiver, tag, agents_data, path, log_path):
        super().__init__(agent)
        self.__result = result
        self.__receiver = receiver
        self.__tag = tag
        self.__agents_data = agents_data
        self.__path = path
        self.log_path = log_path
    
    def action(self):
        message = ACLMessage(ACLMessage.INFORM)
        message.set_ontology('powerflow-result')
        message.set_language('smash-sys')
        message.set_conversation_id(self.__tag)
        message.add_receiver(self.__receiver)
        message.set_content(pickle.dumps({ 'result': self.__result, 'agents': self.__agents_data, 'path': self.__path, 'log_path': self.log_path }))
        self.agent.send(message)
        display_message(self.agent, 'A message with powerflow data was sent to Active Agent')