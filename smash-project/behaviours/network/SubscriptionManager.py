import re
import pickle
import traceback
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import CyclicBehaviour

class SubscriptionManager(CyclicBehaviour):

	"""Este comportamento é responsável por receber as solicitações de agentes que desejam se
	registrar na rede. Responde ao comportamento SubscribeInNetwork."""

	def action(self):

		# Filtros das possíveis mensagens
		connection = Filter()
		connection.set_performative(ACLMessage.PROPOSE)
		connection.set_ontology('subscribe')
		connection.set_language('smash-sys')

		message = self.read()		

		if connection.filter(message):
			try:
				#display_message(self.agent, 'Recebi uma inscrição de {}'.format(message.sender.localname))
				data = pickle.loads(message.content)
				#display_message(self.agent, data)

				# Verifica quem é o solicitante para assim armazenar os dados certos
				if re.search('la_', message.sender.localname):
					self.agent.loadbus_data.append(data)
					#display_message(self.agent, 'Cadastrei o agente de carga {}'.format(data['name']))
					
				elif re.search('sw_', message.sender.localname):
					self.agent.line_data.append(data)
					#display_message(self.agent, 'Cadastrei o agente de chave {}'.format(data['name']))

				else:
					display_message(self.agent, 'Formato não aceito enviado por {}'.format(message.sender.localname))

			except Exception as e:
				display_message(self.agent, '### Bad Status object.')
				traceback.print_exc()