from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.misc.utility import display_message
from pade.behaviours.types import CyclicBehaviour

class RegisterManager(CyclicBehaviour):

	"""Este comportamento tem por objetivo gerenciar as mensagens enviadas pelos agentes que desejam
	se registrar como agente ATIVO e também para a remoção do posto de agente ATIVO."""

	def action(self):

		register = Filter()
		register.set_performative(ACLMessage.REQUEST)
		register.set_protocol('register')

		message = self.read()

		if register.filter(message):

			reply = message.create_reply()

			if message.content == 'register':
				if not self.agent.existsActiveAgent():
					self.agent.registerActiveAgent(message.sender)
					display_message(self.agent, '{} was registered as ACTIVE AGENT.'.format(message.sender.localname))
					reply.set_performative(ACLMessage.CONFIRM)
					self.agent.send(reply)
				else:
					display_message(self.agent, '{} has tried to register, but there is already an active agent registered.'.format(message.sender.localname))
					reply.set_performative(ACLMessage.REFUSE)
					reply.set_content('ACTIVE-AGENT-ALREADY-REGISTERED')
					self.agent.send(reply)
			else:
				if message.content == 'deregister':
					if self.agent.isActiveAgent(message.sender):
						self.agent.deregisterActiveAgent()
						display_message(self.agent, 'Active agent {} was deregistered.'.format(message.sender.localname))
						reply.set_performative(ACLMessage.CONFIRM)
						self.agent.send(reply)
					else:
						display_message(self.agent, 'The agent {} is not allowed to deregister the active agent.'.format(message.sender.localname))
						reply.set_performative(ACLMessage.REFUSE)
						self.agent.send(reply)