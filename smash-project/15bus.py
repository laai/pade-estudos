from pade.acl.aid import AID
from pade.misc.utility import start_loop
from agents.SwitchAgent import SwitchAgent
from agents.LoadAgent import LoadAgent 
from agents.SharingArea import SharingArea
from agents.NetworkAgent import NetworkAgent

if __name__ == '__main__':

	sharing_agent_aid = AID('sharing_agent')
	network_agent_aid = AID('network_agent')

	sharing_agent = SharingArea(sharing_agent_aid)
	network_agent = NetworkAgent(network_agent_aid)

	la_0_aid = AID('la_0')
	la_1_aid = AID('la_1')
	la_2_aid = AID('la_2')
	la_3_aid = AID('la_3')
	la_4_aid = AID('la_4')
	la_5_aid = AID('la_5')
	la_6_aid = AID('la_6')
	la_7_aid = AID('la_7')
	la_8_aid = AID('la_8')
	la_9_aid = AID('la_9')
	la_10_aid = AID('la_10')
	la_11_aid = AID('la_11')
	la_12_aid = AID('la_12')
	la_13_aid = AID('la_13')
	la_14_aid = AID('la_14')
	
	la_0 = LoadAgent(la_0_aid, {'id': 0, 'voltage': 10.58, 'p_set': 0, 'q_set': 0, 'generator': True, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_1 = LoadAgent(la_1_aid, {'id': 1, 'voltage': 11.98, 'p_set': 0.1, 'q_set': 0.06, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_2 = LoadAgent(la_2_aid, {'id': 2, 'voltage': 11.9, 'p_set': 0.09, 'q_set': 0.04, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_3 = LoadAgent(la_3_aid, {'id': 3, 'voltage': 11.88, 'p_set': 0.12, 'q_set': 0.08, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_4 = LoadAgent(la_4_aid, {'id': 4, 'voltage': 11.87, 'p_set': 0.06, 'q_set': 0.03, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_5 = LoadAgent(la_5_aid, {'id': 5, 'voltage': 11.84, 'p_set': 0.06, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_6 = LoadAgent(la_6_aid, {'id': 6, 'voltage': 11.84, 'p_set': 0.2, 'q_set': 0.1, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_7 = LoadAgent(la_7_aid, {'id': 7, 'voltage': 11.97, 'p_set': 0.2, 'q_set': 0.1, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_8 = LoadAgent(la_8_aid, {'id': 8, 'voltage': 11.95, 'p_set': 0.06, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_9 = LoadAgent(la_9_aid, {'id': 9, 'voltage': 11.92, 'p_set': 0.06, 'q_set': 0.02, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_10 = LoadAgent(la_10_aid, {'id': 10, 'voltage': 11.92, 'p_set': 0.045, 'q_set': 0.03, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_11 = LoadAgent(la_11_aid, {'id': 11, 'voltage': 11.85, 'p_set': 0.06, 'q_set': 0.035, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_12 = LoadAgent(la_12_aid, {'id': 12, 'voltage': 11.79, 'p_set': 0.06, 'q_set': 0.035, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_13 = LoadAgent(la_13_aid, {'id': 13, 'voltage': 11.79, 'p_set': 0.12, 'q_set': 0.08, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})
	la_14 = LoadAgent(la_14_aid, {'id': 14, 'voltage': 11.79, 'p_set': 0.06, 'q_set': 0.01, 'generator': False, 'network': network_agent_aid, 'sharing': sharing_agent_aid})

	sw_0 = SwitchAgent('sw_0', {'agent_1': la_0_aid, 'agent_2': la_1_aid, 'resistance': 0.0922, 'reactance': 0.0477,  'status': True, 'network': network_agent_aid})
	sw_1 = SwitchAgent('sw_1', {'agent_1': la_1_aid, 'agent_2': la_2_aid, 'resistance': 0.493, 'reactance': 0.2511,  'status': True, 'network': network_agent_aid})
	sw_2 = SwitchAgent('sw_2', {'agent_1': la_2_aid, 'agent_2': la_3_aid, 'resistance': 0.366, 'reactance': 0.1864,  'status': True, 'network': network_agent_aid})
	sw_3 = SwitchAgent('sw_3', {'agent_1': la_3_aid, 'agent_2': la_4_aid, 'resistance': 0.3811, 'reactance': 0.1941,  'status': True, 'network': network_agent_aid})
	sw_4 = SwitchAgent('sw_4', {'agent_1': la_4_aid, 'agent_2': la_5_aid, 'resistance': 0.819, 'reactance': 0.707,  'status': True, 'network': network_agent_aid})
	sw_5 = SwitchAgent('sw_5', {'agent_1': la_5_aid, 'agent_2': la_6_aid, 'resistance': 0.1872, 'reactance': 0.6188,  'status': True, 'network': network_agent_aid})
	sw_6 = SwitchAgent('sw_6', {'agent_1': la_1_aid, 'agent_2': la_7_aid, 'resistance': 0.164, 'reactance': 0.1565,  'status': True, 'network': network_agent_aid})
	sw_7 = SwitchAgent('sw_7', {'agent_1': la_7_aid, 'agent_2': la_8_aid, 'resistance': 0.409, 'reactance': 0.479,  'status': True, 'network': network_agent_aid})
	sw_8 = SwitchAgent('sw_8', {'agent_1': la_8_aid, 'agent_2': la_9_aid, 'resistance': 1.5042, 'reactance': 1.3554,  'status': True, 'network': network_agent_aid})
	sw_9 = SwitchAgent('sw_9', {'agent_1': la_9_aid, 'agent_2': la_10_aid, 'resistance': 0.4095, 'reactance': 0.4784,  'status': True, 'network': network_agent_aid})
	sw_10 = SwitchAgent('sw_10', {'agent_1': la_2_aid, 'agent_2': la_11_aid, 'resistance': 0.4512, 'reactance': 0.3083,  'status': True, 'network': network_agent_aid})
	sw_11 = SwitchAgent('sw_11', {'agent_1': la_11_aid, 'agent_2': la_12_aid, 'resistance': 0.8980, 'reactance': 0.7091,  'status': True, 'network': network_agent_aid})
	sw_12 = SwitchAgent('sw_12', {'agent_1': la_12_aid, 'agent_2': la_13_aid, 'resistance': 0.896, 'reactance': 0.7011,  'status': True, 'network': network_agent_aid})
	sw_13 = SwitchAgent('sw_13', {'agent_1': la_12_aid, 'agent_2': la_14_aid, 'resistance': 0.2842, 'reactance': 0.1447,  'status': True, 'network': network_agent_aid})
	
	# Tie lines
	sw_14 = SwitchAgent('sw_14', {'agent_1': la_1_aid, 'agent_2': la_11_aid, 'resistance': 1.5, 'reactance': 1.5,  'status': False, 'network': network_agent_aid})
	sw_15 = SwitchAgent('sw_15', {'agent_1': la_4_aid, 'agent_2': la_12_aid, 'resistance': 1.0, 'reactance': 1.0,  'status': False, 'network': network_agent_aid})
	sw_16 = SwitchAgent('sw_16', {'agent_1': la_5_aid, 'agent_2': la_10_aid, 'resistance': 0.5, 'reactance': 0.5,  'status': False, 'network': network_agent_aid})
	sw_17 = SwitchAgent('sw_17', {'agent_1': la_6_aid, 'agent_2': la_14_aid, 'resistance': 0.5, 'reactance': 0.5,  'status': False, 'network': network_agent_aid})

	start_loop([

		sharing_agent, network_agent,

		la_0, la_1,la_2,la_3,la_4,la_5,la_6,la_7,
		la_8,la_9,la_10,la_11,la_12,la_13,la_14,

		sw_0,sw_1,sw_2,sw_3,sw_4,sw_5,
		sw_6,sw_7,sw_8, sw_9,sw_10,sw_11,
		sw_12,sw_13,sw_14,sw_15,sw_16,
		sw_17

		])