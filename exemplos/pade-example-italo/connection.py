from pade.acl.messages import ACLMessage
from pade.acl.messages import AID
from pade.acl.filters import Filter
from pade.behaviours.types import OneShotBehaviour, CyclicBehaviour
from pade.misc.utility import display
import pickle

class ConnectionAcceptor(CyclicBehaviour):
	def action(self):
		f = Filter()
		f.set_performative(ACLMessage.QUERY_IF)
		f.set_protocol('recognition')
		message = self.read()
		if f.filter(message):
			self.agent.add_behaviour(SendData(self.agent, message.content))
			reply = message.create_reply()
			reply.set_performative(ACLMessage.AGREE)
			reply.set_content('I agree the connection.')
			self.agent.send(reply)

class SendData(OneShotBehaviour):
	def __init__(self, agent, name):
		super().__init__(agent)
		self.name = name

	def action(self):
		message = ACLMessage(ACLMessage.SUBSCRIBE)
		message.set_protocol('connection')
		message.set_content(pickle.dumps(self.agent.data()))
		message.add_receiver(AID(self.name))
		self.agent.send(message)

class ConnectNeighbor(CyclicBehaviour):
	def action(self):
		f = Filter()
		f.set_performative(ACLMessage.SUBSCRIBE)
		f.set_protocol('connection')
		message = self.read()
		if f.filter(message):
			data = pickle.loads(message.content)
			self.agent.add_neighbor(data)
			display(self.agent, 'Agent %s connected.' % message.sender.getLocalName())