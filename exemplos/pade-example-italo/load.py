from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.misc.utility import display
from connection import ConnectionAcceptor, ConnectNeighbor
from log import LoadStateShower

class LoadAgent(Agent):
	def __init__(self, name, supply = True, demand = 0.0, voltage = 0.0):
		super().__init__(name)
		self.supply = supply
		self.demand = demand
		self.voltage = voltage
		self.neighbors = list()

	def setup(self):
		display(self, 'I was started. =)')
		self.add_behaviour(ConnectionAcceptor(self))
		self.add_behaviour(ConnectNeighbor(self))
		self.add_behaviour(LoadStateShower(self, 2))

	def add_neighbor(self, neighbor):
		if isinstance(neighbor, dict):
			self.neighbors.append(neighbor)
		else:
			raise Exception('This neighbor not is a LoadAgent')

	def remove_neighbor(self, neighbor):
		if isinstance(neighbor, AID):
			name = neighbor.getLocalName()
		else:
			name = neighbor
		if self.is_neighbor(name):
			self.neighbors.remove(name)

	def is_neighbor(self, name):
		for neighbor in self.neighbors:
			if name == neighbor['name']:
				return True
		return False

	def data(self):
		return {
			'name': self.aid.getLocalName(),
			'supply': self.supply,
			'demand': self.demand,
			'voltage': self.voltage,
		}