from pade.acl.messages import ACLMessage
from pade.acl.messages import AID
from pade.behaviours.types import OneShotBehaviour, SimpleBehaviour
from pade.misc.utility import display

class SendRecognitionMessage(OneShotBehaviour):
	def action(self):
		# Sending message to first one agent
		message = ACLMessage(ACLMessage.QUERY_IF)
		message.set_protocol('recognition')
		message.add_receiver(AID(self.agent.names[0]))
		message.set_content(self.agent.names[1])
		self.agent.send(message)
		display(self.agent, 'Recognition message sent to %s' % self.agent.names[0])
		# Sending message to second one agent
		message.reset_receivers()
		message.add_receiver(AID(self.agent.names[1]))
		message.set_content(self.agent.names[0])
		self.agent.send(message)
		display(self.agent, 'Recognition message sent to %s' % self.agent.names[1])

class ConfirmConnection(SimpleBehaviour):
	def __init__(self, agent):
		super().__init__(agent)
		self.counter = 0

	def action(self):
		message = self.read()
		if message.sender.getLocalName() in self.agent.names and message.performative == ACLMessage.AGREE:
			display(self.agent, 'The agent %s accepted the connection' % message.sender.getLocalName())
			self.counter += 1

	def done(self):
		return self.counter >= 2

	def on_end(self):
		display(self.agent, 'The connection process is done.')