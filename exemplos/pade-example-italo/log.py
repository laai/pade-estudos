from pade.behaviours.types import TickerBehaviour
from pade.misc.utility import display_message

class LoadStateShower(TickerBehaviour):
	def on_tick(self):
		show = ''
		for neighbor in self.agent.neighbors:
			show += str(neighbor) + '\n'
		display_message(self.agent, 'INFORMATION FROM {agent}:\n {data} '.format(
			agent = self.agent.aid.getLocalName(),
			data = show)
		)

class SwitchStateShower(TickerBehaviour):
	def on_tick(self):
		show = ''
		for node in self.agent.nodes:
			show += str(node) + '\n'
		display_message(self.agent, 'INFORMATION FROM {agent}:\n {data} '.format(
			agent = self.agent.aid.getLocalName(),
			data = show)
		)