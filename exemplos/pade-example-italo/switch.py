from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.misc.utility import display
from load import LoadAgent
from recognition import SendRecognitionMessage, ConfirmConnection
from log import SwitchStateShower

class SwitchAgent(Agent):
	def __init__(self, name, nodes, state = True, resistance = 0.0):
		super().__init__(name)
		self.state = state
		self.resistance = resistance
		self.nodes = list()
		self.names = nodes

	def setup(self):
		display(self, 'I was started. =)')
		self.add_behaviour(SendRecognitionMessage(self))
		self.add_behaviour(ConfirmConnection(self))
		self.add_behaviour(SwitchStateShower(self, 2))

	def add_node(self, loadagent):
		if isinstance(loadagent, dict):
			if len(self.nodes) < 2:
				self.nodes.append(loadagent)
			else:
				raise Exception('SwitchAgent aready have two nodes connecteds.')
		else:
			raise Exception('This node not is a LoadAgent.')

	def remove_node(self, node):
		if isinstance(node, AID):
			name = node.getLocalName()
		else:
			name = node
		if self.is_node(name):
			self.nodes.remove(name)

	def is_node(self, name):
		for node in self.nodes:
			if name == node['name']:
				return True
		return False

	def data(self):
		return {
			'name': self.aid.getLocalName(),
			'state': self.state,
			'resistance': self.resistance,
			#'nodes': [self.nodes[0]['name'], self.nodes[1]['name']]
		}