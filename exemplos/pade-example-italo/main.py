from pade.misc.utility import start_loop
from load import LoadAgent
from switch import SwitchAgent

if __name__ == '__main__':
	agents = list()
	agents.append(LoadAgent('ac0', demand = 100, voltage = 127))
	agents.append(LoadAgent('ac1', demand = 200, voltage = 220))
	agents.append(LoadAgent('ac2', demand = 400, voltage = 215))
	agents.append(LoadAgent('ac3', demand = 450, voltage = 180))
	agents.append(SwitchAgent('sw-01', ['ac0', 'ac1'], resistance = 0.7))
	agents.append(SwitchAgent('sw-02', ['ac1', 'ac2'], resistance = 0.3))
	agents.append(SwitchAgent('sw-03', ['ac1', 'ac3'], resistance = 0.5))
	start_loop(agents)