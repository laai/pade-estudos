from pade.misc.utility import display_message, start_loop
from pade.core.agent import Agent
from pade.acl.messages import ACLMessage
from pade.acl.aid import AID
from pade.behaviours.protocols import TimedBehaviour

class ComportTemporal(TimedBehaviour):
    
    """ Comportamento temporal do agente """
    
    def __init__(self, agent, time):
        super(ComportTemporal, self).__init__(agent, time)

    def on_time(self):
        super(ComportTemporal, self).on_time()
        display_message(self.agent.aid.localname, 'Olá, eu sou um agente temporal!')
        
        
class AgenteTemporal(Agent):
    
    def __init__(self, aid):
        super(AgenteTemporal, self).__init__(aid=aid, debug=False)
        
        # O tempo aqui é dado em segundos, portanto, neste exemplo temos um ação que ocorre a cada 2s
        self.comport_temp = ComportTemporal(self, 2.0)
        
        self.behaviours.append(self.comport_temp)
        
class AgenteTemporal2(Agent):
    
    def __init__(self, aid):
        super(AgenteTemporal2, self).__init__(aid=aid, debug=False)
        
        # O tempo aqui é dado em segundos, portanto, neste exemplo temos um ação que ocorre a cada 2s
        self.comport_temp = ComportTemporal(self, 7.0)
        
        self.behaviours.append(self.comport_temp)

if __name__ == '__main__':

    agentes = list()
    
    nome1 = 'Agente_Temporal@localhost:4000'
    agente_l = AgenteTemporal(AID(name=nome1))
    agentes.append(agente_l)
    
    nome2 = 'Agente_Temporal_2@localhost:4001'
    agente_2 = AgenteTemporal2(AID(name=nome2))
    agentes.append(agente_2)

    start_loop(agentes)
