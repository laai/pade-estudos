from pade.misc.utility import display_message
from pade.core.agent import Agent
from pade.drivers.mosaik_driver import MosaikCon

import pandas as pd
import numpy as np
import json
import pickle
import random

MOSAIK_MODELS = {
    'api_version': '2.2',
    'models': {
        'DeviceAgent': {
            'public': True,
            'params': [],
            'attrs': ['P', 'node_id'],
        },
    },
}


class MosaikSim(MosaikCon):

    def __init__(self, agent):
        super(MosaikSim, self).__init__(MOSAIK_MODELS, agent)
        self.entities = list()

    def create(self, num, model, init_val, medium_val):
        self.entities_info = list()

        for i in range(num):
            self.entities.append(init_val)
            display_message(self.agent, str(init_val))
            entities_info.append({'eid': self.sim_id, 'type': model, 'rel': []})
        
        return entities_info

    def step(self, time, inputs):
        
        if time % 501 == 0 and time != 0: # a cada 5 min
            display_message(self.agent, '{:4d}'.format(time))
        return time + self.step_size

    def get_data(self, outputs):
        response = dict()
        for model, list_values in outputs.items():
            response[model] = dict()
            for value in list_values:
                response[model][value] = 1.0
        return response


class DeviceAgent(Agent):

    def __init__(self, aid, node_id):

        super(DeviceAgent, self).__init__(aid=aid, debug=False)
        self.node_id = node_id
        self.mosaik_sim = MosaikSim(self)
        self.dm_curve = np.zeros(50)
        self.clear_price = None

        # open the config.json file with some important informations about
        # the device characteristics, read and store this information.
        config = json.load(open('config.json'))

        '''This part of code create a dictionary like this:

            {'stochastic_gen': {'power': 5.41, 'status': None, 'demand': None},
            'shiftable_load': {'power': 2.02, 'status': None, 'demand': None},
            'buffering_device': {'power': 2.1, 'status': None, 'demand': None},
            'user_action_device': {'power': 5.55, 'status': None, 'demand': None}}

        '''
        self.device_dict = dict()
        for device_type, device_info in config['devices'].items():
            if str(self.node_id) in device_info['powers'].keys():
                self.device_dict[device_type] = {'power': device_info['powers'][str(self.node_id)],
                                                 'status': None,
                                                 'demand': None}
        # print(self.aid.name)
        # print(self.device_dict)