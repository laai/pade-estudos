import itertools
import random
import json

from mosaik.util import connect_randomly, connect_many_to_one
import mosaik

sim_config = {

	'ExampleSim': {'python': example_sim:ExampleSim}
	'PadeSim': {'connect': '127.0.0.1:20000'}
    
}