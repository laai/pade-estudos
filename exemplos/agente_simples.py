from pade.misc.utility import display_message, start_loop
from pade.core.agent import Agent
from pade.acl.messages import ACLMessage
from pade.acl.aid import AID
from pade.behaviours.protocols import Behaviour

class HelloWorld(Behaviour):

    "Comportamento do agente"

    def __init__(self, agent):
        super(HelloWorld, self).__init__(agent=agent)
        
    def on_start(self):
        display_message(self.agent.aid.localname, 'Hello World!')
        

class AgenteSimples(Agent):

    def __init__(self, aid):
        
        super(AgenteSimples, self).__init__(aid=aid, debug=False)
        
        self.comport_hello = HelloWorld(self)
        
        self.behaviours.append(self.comport_hello)
        
        if self.behaviours == None:
            print("O comportamento foi executado e retirado da lista")
        else:
            print("O comportamento continua na lista")
        

if __name__ == '__main__':

    agentes = list()
    
    agent_name = 'Agente_Simples@localhost:9000'
    agente = AgenteSimples(AID(name=agent_name))
    agentes.append(agente)
    
    start_loop(agentes)
