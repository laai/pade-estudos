import mosaik
import mosaik_pypower.mosaik
from mosaik.util import connect_randomly, connect_many_to_one

"""
sim_config = {
    'PyPowerSim': {'python': 'test_mosaik.mosaik:PyPower'},
    'PadeSim': {'connect': 'localhost:2000'}
}"""

sim_config = {
     'PyPower': {
        'python': 'mosaik_pypower.mosaik:PyPower',
        # 'cmd': 'mosaik-pypower %(addr)s',
    },
    'WebVis': {
        'cmd': 'mosaik-web -s 0.0.0.0:2000 %(addr)s',
    },
}

START = '2019-01-01 00:00:00'
END = 2 * 24 * 3600  # 1 day


def create_scenario(world):

    #sim = world.start('PyPower', step_size=60)

    pypower = world.start('PyPower', step_size=60)

    grid = pypower.Grid(gridfile='test_case_b.json').children

    """nodes = [e for e in grid if e.type in ('RefBus, PQBus')]
    #connect_many_to_one(world, nodes, 'P', 'Q')

    branches = [e for e in grid if e.type in ('Transformer', 'Branch')]
    #connect_many_to_one(world, branches,'P_from', 'Q_from', 'P_to', 'P_from')"""
    trafo_nodes = []
    # 1. Get all transformers:
    trafos = [e for e in grid if e.type == 'Transformer']
    # 2. Group PQBuses by full ID for easier access:
    nodes = {e.full_id: e for e in grid if e.type == 'PQBus'}
    # 3. Iterate transformer relations (assuming that they are connected to the
    #    RefBus on their primary side):
    for trafo in trafos:
       rels = world.entity_graph[trafo.full_id]
       assert len(rels) == 2
       for rel in rels:
             if rel in nodes:
                trafo_nodes.append(nodes[rel])
                break
       else:
             raise ValueError('No PQBus found at trafo.')

    """
    grid = sim.Grid(gridfile='test_case_b.json')
    grid = grid.children

    pvs = make_pvs(...)  # Returns a list of PV entities
    pq_buses = [e for e in grid if e.type == 'PQBus']
    mosaik.util.connect_randomly(world, pq_buses, ('P_out', 'P'), ('Q_out', 'Q'))"""


world = mosaik.World(sim_config)
create_scenario(world)
world.run(until=50000)